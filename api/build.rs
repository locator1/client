//! This script generates a rust api from an openapi schema.
//!
//! This requires `openapi-generator` to be installed.
use std::fs;
use std::process::Command;
use std::env;
use std::path::PathBuf;

static BYTE_DEFINITION: &str = "\
    Byte:
      type: integer
      format: byte
      minimum: 0x00
      maximum: 0xFF
";

static URLENCODE_DEFINITION: &str = "\
pub fn urlencode<T: AsRef<str>>(s: T) -> String {
    ::url::form_urlencoded::byte_serialize(s.as_ref().as_bytes()).collect()
}
";

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();

    // Load the document.
    let doc = include_str!("openapi.yaml");

    println!("Replacing Bytes...");

    // Now we replace all the `Byte` types with a custom type which
    // corresponds to the rust type of u8.
    let doc = doc.replace(
        r##"$ref: "#/components/schemas/Byte""##,
        "type: Byte"
    );
    let doc = doc.replace(
        BYTE_DEFINITION,
        ""
    );

    println!("Replaced Bytes.");

    // Now we write out this file
    let mut openapi_temp = PathBuf::from(&out_dir);
    openapi_temp.push("openapi.yaml.temp");

    println!("Writing temporary document to {:?}...", openapi_temp);
    fs::write(&openapi_temp, doc.as_bytes()).unwrap();

    println!("Writing temporary document...");
    println!("Running open api generator to {:?}...", out_dir);

    let output = Command::new("openapi-generator")
        .arg("generate")
        .arg("-g").arg("rust")
        .arg("-i").arg(&openapi_temp)
        .arg("--type-mappings=Byte=u8")
        .arg("--additional-properties=packageName=open_gps_api,library=reqwest")
        .arg("-o").arg(&out_dir)
        .output()
        .unwrap();

    if !output.status.success() {
        println!("[ERROR] Failed to run openapi-generator.");
        println!("{}", String::from_utf8(output.stdout).unwrap());
        println!("{}", String::from_utf8(output.stderr).unwrap());
    }
    

    println!("Ran open api generator.");

    println!("Removing source directory...");
    drop(fs::remove_dir_all("src/api"));
    println!("Removed source directory.");

    let mut out_dir_src = PathBuf::from(&out_dir);
    out_dir_src.push("src");

    println!("Copying from temp source ({:?}) to final source...", out_dir_src);
    fs::rename(out_dir_src, "src/api").unwrap();
    println!("Copied from temp source to final source.");

    println!("Renaming lib.rs to mod.rs...");
    fs::rename("src/api/lib.rs", "src/api/mod.rs").unwrap();
    println!("Renamed lib.rs to mod.rs.");

    println!("Removing extern crates...");
    let lib = String::from_utf8(fs::read("src/api/mod.rs").unwrap()).unwrap();
    let lib = lib.replace("extern crate", "//extern crate");
    let lib = lib.replace("#[macro_use]", "//#[macro_use]");
    fs::write("src/api/mod.rs", lib).unwrap();
    println!("Removed extern crates.");

    println!("Injecting urlencoded...");
    println!("This is done because by default the method of escaping spaces in a url is wrong in this generator.");
    let api_mod = String::from_utf8(fs::read("src/api/apis/mod.rs").unwrap()).unwrap();
    let api_mod = api_mod.replace(URLENCODE_DEFINITION, "pub use urlencoding::encode as urlencode;");
    fs::write("src/api/apis/mod.rs", api_mod).unwrap();
    println!("Injected urlencoded.");

    println!("Done!");
}
