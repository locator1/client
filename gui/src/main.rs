#![deny(unused_must_use)]
#![warn(clippy::pedantic)]
#![allow(
    clippy::module_name_repetitions,
    clippy::non_ascii_literal,
    clippy::missing_panics_doc,
    clippy::missing_errors_doc
)]

use adw::prelude::*;
use gtk::Application;

pub mod ui;
#[macro_use]
pub mod util;
pub(crate) use util::macros::resource;

pub static ID: &str = "org.john_t.Locator";

#[allow(clippy::semicolon_if_nothing_returned)]
#[tokio::main]
async fn main() {
    env_logger::init();

    // Setup resources
    setup_resources();

    // Create a new application
    let app = Application::builder()
        .application_id(ID)
        .flags(gtk::gio::ApplicationFlags::HANDLES_OPEN)
        .build();

    // Connect the activate signal
    app.connect_activate(|app| {
        let _ = ui::start(app);
    });

    // Run
    app.run();
}

fn setup_resources() {
    gtk::gio::resources_register_include!("compiled.gresource").unwrap();
}
