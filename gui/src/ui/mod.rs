pub mod main;
pub mod setup;

use common::tasks::{self, TaskMode};
use glib::{clone, MainContext};
use gtk::glib;
use gtk::Application;

/// Starts the program.
///
/// This involves seeing if we need to run through the setup wizard, and
/// then launching a ui.
///
/// This function would be async if I could make it so, but unfortunately
/// if you spawn gtk things in an async function gtk will segfault so 🤔.
#[must_use]
pub fn start(app: &Application) -> Option<()> {
    // Get the async runtime
    let mc = MainContext::default();

    // Create a TaskMode
    let tm = mc.block_on(TaskMode::new());
    let in_main = mc.block_on(clone! {
        @strong tm =>
        async move {
            tasks::misc::has_key(&tm).await.unwrap_or(false)
        }
    });

    if in_main {
        main::build_ui(app, &tm);
        Some(())
    } else {
        setup::build_ui(app, &tm);
        Some(())
    }
}
