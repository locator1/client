use adw::prelude::*;
use common::error::Error;
use common::tasks;
use common::tasks::TaskMode;
use common::DecryptedGps;
use futures::StreamExt;
use geoclue::{LocationProxy, ManagerProxy};
use gladis as gladis4;
use gladis::Gladis;
use gtk::glib::{self, clone, MainContext};
use gtk::Application;

use super::Ui;
use crate::resource;
use crate::util::LogErrToLabel;

pub fn init(app: &Application, tm: &TaskMode, ui: &Ui) {
    let mc = MainContext::default();

    // Set up the listbox
    mc.spawn_local(clone!(
        @strong tm,
        @weak app,
        @strong ui => async move {
            let res = init_listbox(&app, &tm, &ui).await;
            res.log_to_dialog_with_parent(
                Some(&app),
                "Error with outgoing connections",
                &ui.window
            );
        }
    ));

    // Set up adding connections
    ui.outgoing_add.connect_clicked(clone!(
        @strong tm,
        @weak app,
        @strong ui => move |_| add_connection_dialog(&app, &tm, &ui)
    ));

    mc.spawn_local(clone!(
        @weak app,
        @strong ui,
        @strong tm => async move {
            let res = routine(&tm, &app, &ui).await;
            res.log_to_dialog_with_parent(Some(&app), "Error sending outgoing", &ui.window);
        }
    ));
}

async fn init_listbox(
    app: &Application,
    tm: &TaskMode,
    ui: &Ui,
) -> Result<(), Error> {
    // Remove everything
    while let Some(row) = ui.outgoing_listbox.row_at_index(0) {
        ui.outgoing_listbox.remove(&row);
    }

    let (username, mut conns) = tokio::try_join!(
        tasks::config::get_username(tm),
        tasks::list_connections(tm),
    )?;

    let _username_r = username.as_ref();
    conns.retain(|conn| Some(&conn.reciever) != username.as_ref());

    futures::future::try_join_all(conns.into_iter().map(|conn| async move {
        let connui = create_listbox_row(tm, app, ui, &conn.reciever);
        ui.outgoing_listbox.append(&connui.container);

        Ok::<(), Error>(())
    }))
    .await?;

    Ok(())
}

#[derive(Gladis, Clone)]
struct ListboxRowUi {
    pub container: gtk::Box,
    pub outgoing: gtk::Label,
    pub delete: gtk::Button,
}

fn create_listbox_row(
    tm: &TaskMode,
    app: &Application,
    app_ui: &Ui,
    conn: &str,
) -> ListboxRowUi {
    let ui = ListboxRowUi::from_string(resource!("ui/outgoing.ui.xml"))
        .expect("Failed to load UI");

    // Set the username
    ui.outgoing.set_label(conn);

    // Make it deletable
    config_delete_button(tm, app, app_ui, &ui.delete, conn);

    ui
}

fn config_delete_button(
    tm: &TaskMode,
    app: &Application,
    ui: &Ui,
    button: &gtk::Button,
    conn: &str,
) {
    let conn = conn.to_string();
    button.connect_clicked(clone!(
        @strong tm,
        @weak app,
        @strong ui => move |_button| {
            let mc = MainContext::default();
            mc.spawn_local(clone!(@strong tm, @weak app, @strong ui, @strong conn => async move {
                let res = delete_account(&app, &tm, &ui, &conn).await;
                res.log_to_dialog_with_parent(
                    Some(&app),
                    "Error deleting connection",
                    &ui.window
                );
            }));
        }
    ));
}

async fn delete_account(
    app: &Application,
    tm: &TaskMode,
    ui: &Ui,
    conn: &str,
) -> Result<(), Error> {
    // We must confirm that the user wants to do this.
    let dialog = gtk::MessageDialog::new(
        Some(&ui.window),
        gtk::DialogFlags::MODAL,
        gtk::MessageType::Question,
        gtk::ButtonsType::None,
        "Are you sure you want to delete the connection?",
    );

    dialog.add_buttons(&[
        ("Cancel", gtk::ResponseType::Cancel),
        ("Delete", gtk::ResponseType::DeleteEvent),
    ]);

    let answer = dialog.run_future().await;
    dialog.close();

    // Check their answer
    match answer {
        gtk::ResponseType::DeleteEvent => {
            tasks::remove_connection_from_reciever(tm, conn).await?;
            init_listbox(app, tm, ui).await?;
            Ok(())
        }
        _ => Ok(()),
    }
}

#[derive(Gladis, Clone)]
struct AddConnectionUi {
    pub window: gtk::Window,
    pub cancel: gtk::Button,
    pub create: gtk::Button,
    pub entry: gtk::Entry,
}

fn add_connection_dialog(app: &Application, tm: &TaskMode, app_ui: &Ui) {
    // Load the UI
    let ui =
        AddConnectionUi::from_string(resource!("ui/add_connection.ui.xml"))
            .expect("Failed to load UI");

    // Integrate it
    ui.window.set_transient_for(Some(&app_ui.window));
    ui.window.present();

    ui.cancel.connect_clicked(clone!(
        @strong ui => move |_| ui.window.destroy()
    ));

    ui.create.connect_clicked(clone!(
        @strong tm,
        @strong ui,
        @weak app,
        @strong app_ui => move |_| {
            let mc = MainContext::default();
            mc.spawn_local(
                clone! (
                    @strong tm,
                    @strong ui,
                    @weak app,
                    @strong app_ui => async move {
                        ui.window.destroy();
                        let res = add_connection(&app, &tm, &ui, &app_ui).await;
                        res.log_to_dialog_with_parent(
                            Some(&app),
                            "Error adding outgoing connection",
                            &ui.window
                        );
                    }
                )
            );
        }
    ));
}

async fn add_connection(
    app: &Application,
    tm: &TaskMode,
    ui: &AddConnectionUi,
    app_ui: &Ui,
) -> Result<(), Error> {
    tasks::add_connection(tm, &*ui.entry.text()).await?;
    init_listbox(app, tm, app_ui).await?;
    Ok(())
}

pub async fn routine(
    tm: &TaskMode,
    app: &Application,
    ui: &Ui,
) -> Result<(), Error> {
    // Get the connections
    let (username, mut conns) = tokio::try_join!(
        tasks::config::get_username(tm),
        tasks::list_connections(tm),
    )?;

    let _username_r = username.as_ref();
    conns.retain(|conn| Some(&conn.reciever) != username.as_ref());

    // Create a geoclue client
    let conn = zbus::Connection::system().await?;
    let manager = ManagerProxy::new(&conn).await?;
    let client = manager.get_client().await?;

    // Configure the client
    client.set_desktop_id(common::ID).await?;
    client
        .set_requested_accuracy_level(geoclue::AccuracyLevel::Exact)
        .await?;

    // Start waiting for changes in the location
    let mut location_updated = client.receive_location_updated().await?;
    client.start().await?;

    let mut conns = conns.iter().map(|x| x.reciever.as_str());

    // Loop through the location
    while let Some(signal) = location_updated.next().await {
        let res = send(signal, tm, conns.by_ref().clone(), &conn).await;
        res.log_to_dialog_with_parent(
            Some(app),
            "Error sending outgoing",
            &ui.window,
        );
    }

    Ok(())
}

pub async fn send(
    signal: geoclue::LocationUpdated,
    tm: &TaskMode,
    receivers: impl Iterator<Item = &str>,
    conn: &zbus::Connection,
) -> Result<bool, Error> {
    // Get the args to create a location
    let args = signal.args()?;

    log::info!("Getting location...");

    // Create the location
    let location = LocationProxy::builder(conn)
        .path(args.new())?
        .build()
        .await?;

    log::info!("Got location.");

    log::info!("Getting location properties...");
    let (longitude, latitude, accuracy) = tokio::try_join!(
        location.longitude(),
        location.latitude(),
        location.accuracy(),
    )?;

    log::info!("Got location properties.");

    // Check if the accuracy specified is okay.
    if accuracy >= f64::INFINITY {
        log::info!("Retrying because of inaccuracy...");
        return Ok(true);
    }

    log::info!("Looping through receivers...");
    // Send the location.
    for receiver in receivers {
        log::debug!("Sending to {}...", receiver);
        tasks::gps::send(
            tm,
            DecryptedGps {
                longitude: Some(longitude),
                latitude: Some(latitude),
                accuracy: Some(accuracy),
                date: None,
            },
            receiver,
        )
        .await?;
    }
    log::info!("Send to all receivers.");

    Ok(false)
}
