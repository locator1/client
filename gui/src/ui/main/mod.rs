use adw::prelude::*;
use common::tasks::TaskMode;
use gladis as gladis4;
use gladis::Gladis;
use gtk::{
    gio,
    glib::{self, clone, MainContext},
    Application,
};

use crate::resource;

mod about;
mod incoming;
mod logout;
mod outgoing;

#[derive(Gladis, Clone)]
pub struct Ui {
    pub window: gtk::ApplicationWindow,
    pub incoming_listbox: gtk::ListBox,
    pub outgoing_listbox: gtk::ListBox,
    pub outgoing_add: gtk::Button,
}

pub fn build_ui(app: &Application, tm: &TaskMode) {
    // Start the builder
    let ui = Ui::from_string(resource!("ui/main_interface.ui.xml"))
        .expect("Failed to load UI");

    incoming::init(app, tm, &ui);
    outgoing::init(app, tm, &ui);

    // Set up auxillary actions
    setup_menu(app, tm, &ui);

    // Present the window, better to be responsive than complete?
    ui.window.set_application(Some(app));
    ui.window.present();
}

fn setup_menu(app: &Application, tm: &TaskMode, ui: &Ui) {
    // Sets up `app.about`
    let about_action = gio::SimpleAction::new("about", None);
    about_action.connect_activate(clone!(
        @weak app,
        @strong ui => move |_, _|
        about::show_about_dialog(&app, &ui.window)
    ));
    app.add_action(&about_action);

    // Set up logout
    let logout_action = gio::SimpleAction::new("logout", None);
    logout_action.connect_activate(clone!(
        @weak app,
        @strong tm,
        @strong ui => move |_, _| {
            let mc = MainContext::default();
            mc.spawn_local(clone!(
                @weak app,
                @strong tm,
                @strong ui => async move {
                    logout::setup_logout(&app, &tm, &ui.window).await;
                }
            ));
        }
    ));
    app.add_action(&logout_action);
}
