use adw::prelude::*;
use chrono::prelude::*;
use common::error::Error;
use common::gps::DecryptedGps;
use common::tasks;
use common::tasks::TaskMode;
use gladis as gladis4;
use gladis::Gladis;
use gtk::gio;
use gtk::glib::{self, clone, MainContext};
use gtk::Application;
use once_cell::sync::Lazy;

use super::Ui;
use crate::resource;
use crate::util::LogErrToLabel;

static TIMEAGO_FORMATTER: Lazy<timeago::Formatter> =
    Lazy::new(timeago::Formatter::new);

pub fn init(app: &Application, tm: &TaskMode, ui: &Ui) {
    let mc = MainContext::default();
    // Set up the listbox
    mc.spawn_local(clone!(
        @strong tm,
        @weak app,
        @strong ui => async move {
            let res = init_listbox(&app, &tm, &ui).await;
            dbg!(&res);
            res.log_to_dialog_with_parent(
                Some(&app),
                "Error with incoming connections",
                &ui.window
            );
        }
    ));

    // Reload when tapped upon
    ui.incoming_listbox.connect_row_activated(clone!(
        @weak app,
        @strong ui,
        @strong tm => move |_listbox, row| {
            let mc = MainContext::default();
            mc.spawn_local(clone!(
                @weak app,
                @strong ui,
                @weak row,
                @strong tm => async move {
                    let grid = row.child().unwrap().dynamic_cast().unwrap();

                    let res = update_listbox_row(&app, &ui.window, &tm, &grid).await;
                    res.log_to_dialog_with_parent(
                        Some(&app),
                        "Error with incoming connections",
                        &ui.window
                    );
                }
            ));
        }
    ));
}

async fn init_listbox(
    app: &Application,
    tm: &TaskMode,
    ui: &Ui,
) -> Result<(), Error> {
    let (username, mut conns) = tokio::try_join!(
        tasks::config::get_username(tm),
        tasks::list_connections(tm),
    )?;

    let now = Utc::now();

    conns.retain(|conn| Some(&conn.sender) != username.as_ref());

    futures::future::try_join_all(conns.into_iter().map(|conn| async move {
        let tm = &tm;
        let now = &now;

        let gps = tasks::gps::get(tm, &conn.sender).await?;
        let connui =
            create_listbox_row(app, &ui.window, &conn.sender, gps, now);
        ui.incoming_listbox.append(&connui.grid);

        Ok::<(), Error>(())
    }))
    .await?;

    Ok(())
}

#[derive(Gladis, Clone)]
struct ListboxRowUi {
    pub grid: gtk::Grid,
    pub incoming: gtk::Label,
    pub pos: gtk::Label,
    pub open: gtk::Button,
}

fn create_listbox_row(
    app: &Application,
    win: &gtk::ApplicationWindow,
    conn: &str,
    gps: DecryptedGps,
    now: &chrono::DateTime<Utc>,
) -> ListboxRowUi {
    let ui = ListboxRowUi::from_string(resource!("ui/incoming.ui.xml"))
        .expect("Failed to load UI");

    // Set the username
    ui.incoming.set_text(conn);

    // Set the location
    ui.pos.set_text(&format!(
        "Last seen {} at ({}, {})",
        gps.date.as_ref().map_or_else(
            || String::from("???"),
            |date| TIMEAGO_FORMATTER.convert_chrono(*date, *now)
        ),
        gps.longitude
            .as_ref()
            .map_or_else(|| String::from("???"), |f| format!("{}", f)),
        gps.latitude
            .as_ref()
            .map_or_else(|| String::from("???"), |f| format!("{}", f)),
    ));

    // Make it openable
    config_open_button(app, win, &ui.open, gps);

    ui
}

fn config_open_button(
    app: &Application,
    win: &gtk::ApplicationWindow,
    button: &gtk::Button,
    gps: DecryptedGps,
) {
    button.connect_clicked(
        clone!(@weak app, @weak win => move |_button| {
            open(&gps)
                .log_to_dialog_with_parent(Some(&app), "Error opening in maps", &win);
        }
    ));
}

/// Updates the location on a specific row.
async fn update_listbox_row(
    app: &Application,
    win: &gtk::ApplicationWindow,
    tm: &TaskMode,
    grid: &gtk::Grid,
) -> Result<(), Error> {
    // Get the time.
    let now = Utc::now();

    // Get the UI components
    let children = grid.observe_children();

    let sender_label: gtk::Label =
        children.item(0).unwrap().dynamic_cast().unwrap();

    let pos: gtk::Label = children.item(1).unwrap().dynamic_cast().unwrap();

    let button: gtk::Button = children.item(2).unwrap().dynamic_cast().unwrap();

    // Get the coordinats
    let gps = tasks::gps::get(tm, &*sender_label.label()).await?;

    // Get the name of the place
    let nominatim;
    if let (Some(lon), Some(lat)) = (gps.longitude, gps.latitude) {
        nominatim =
            Some(tasks::nominatim::reverse_geocode(tm, lon, lat).await?);
    } else {
        nominatim = None;
    }

    // Set the label up
    pos.set_text(&format!(
        "Last seen {} at {} ({}, {})",
        gps.date.as_ref().map_or_else(
            || String::from("???"),
            |date| TIMEAGO_FORMATTER.convert_chrono(*date, now)
        ),
        nominatim.map_or_else(String::new, |x| x
            .display_name
            .unwrap_or_else(String::new)),
        gps.longitude
            .as_ref()
            .map_or_else(|| String::from("???"), |f| format!("{}", f)),
        gps.latitude
            .as_ref()
            .map_or_else(|| String::from("???"), |f| format!("{}", f)),
    ));

    // Fix the button
    config_open_button(app, win, &button, gps);

    Ok(())
}

pub fn open(gps: &DecryptedGps) -> Result<(), Error> {
    use std::io;

    // Launch it with no context.
    gio::AppInfo::launch_default_for_uri(
        &format!(
            "geo:{},{}",
            gps.latitude.unwrap_or(0.0),
            gps.longitude.unwrap_or(0.0),
        ),
        Option::<&gio::AppLaunchContext>::None,
    )
    .map_err(|x| io::Error::new(io::ErrorKind::Other, x))?;

    Ok(())
}
