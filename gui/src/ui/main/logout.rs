use adw::prelude::*;
use common::tasks::{self, TaskMode};
use gtk::glib::{self, clone, MainContext};
use gtk::Application;

use crate::util::LogErrToLabel;

pub async fn setup_logout(
    app: &Application,
    tm: &TaskMode,
    window: &impl IsA<gtk::Window>,
) {
    // Present a message dialog
    let dialog = gtk::MessageDialog::builder()
        .message_type(gtk::MessageType::Question)
        .text("Logout?")
        .secondary_text(
            "Are you sure you want to logout?\n\
            Doing this will invalidate your session.\n\
            ",
        )
        .transient_for(window)
        .build();

    dialog.add_button("Cancel", gtk::ResponseType::Cancel);
    dialog.add_button("Logout", gtk::ResponseType::Yes);
    dialog.set_default_response(gtk::ResponseType::Cancel);

    dialog.run_async(clone!(
        @weak app, @strong window, @weak dialog, @strong tm => move |_, answer| {
            dialog.close();
            let mc = MainContext::default();
            mc.spawn_local(clone!(
                @weak app,
                @strong window,
                @strong tm,
                @strong answer => async move {
                    logout_ui(&app, &tm, &window, answer).await;
                }
            ));
        }
    ));
}

pub async fn logout_ui(
    app: &Application,
    tm: &TaskMode,
    window: &impl IsA<gtk::Window>,
    answer: gtk::ResponseType,
) -> Option<()> {
    match answer {
        gtk::ResponseType::Yes => {
            logout(app, tm, window).await?;
            window.close();
        }
        gtk::ResponseType::Cancel => (),
        _ => unreachable!(),
    }

    Some(())
}

pub async fn logout(
    app: &Application,
    tm: &TaskMode,
    window: &impl IsA<gtk::Window>,
) -> Option<()> {
    // Remove the file
    tasks::config::logout(tm).await.log_to_dialog_with_parent(
        Some(app),
        "Error logging out",
        window,
    )?;

    Some(())
}
