use adw::prelude::*;
use gtk::Application;
use gtk::Window;

pub fn show_about_dialog(app: &Application, parent: &impl IsA<Window>) {
    let about_dialog = gtk::AboutDialog::builder()
        .authors(vec!["John Toohey <john_t@mailo.com>".to_string()])
        .comments("A program to share gps location with friends!")
        .copyright("John Toohey")
        .license_type(gtk::License::Gpl30)
        .logo_icon_name(crate::ID)
        .program_name("Locator")
        .version(env!("CARGO_PKG_VERSION"))
        .website("https://gitlab.com/locator1/client-gtk-edition")
        .application(app)
        .transient_for(parent)
        .build();
    about_dialog.present();
}
