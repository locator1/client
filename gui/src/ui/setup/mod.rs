pub mod connections;
pub mod finish;
pub mod login;
pub mod server;
pub mod signup;

use adw::prelude::*;
use adw::ApplicationWindow;
use common::tasks::TaskMode;
pub use connections::setup_connections;
pub use finish::setup_finish;
use gtk::glib::{self, clone};
use gtk::Application;
pub use login::setup_login;
pub use server::setup_server;
pub use signup::setup_signup;

use crate::resource;

pub fn build_ui(app: &Application, tm: &TaskMode) {
    // Start the builder
    let builder = gtk::Builder::from_string(resource!("ui/setup.ui.xml"));

    // Get the window
    let window: ApplicationWindow =
        builder.object("window").expect("Cannot find the window :/");

    let back: gtk::Button = builder.object("back").expect("Can't find back.");

    let _finish: gtk::Button =
        builder.object("finish").expect("Can't find back.");

    // Gets a stack
    let stack: adw::ViewStack =
        builder.object("stack").expect("Can't find stack.");

    back.connect_clicked(clone!(
        @weak stack => move |_| go_back(&stack)
    ));

    stack.connect_visible_child_name_notify(clone!(
        @weak back =>
        move |stack|
            back.set_sensitive(stack.visible_child_name().as_deref() != Some("server_config"));
    ));

    setup_server(tm, &stack, &builder);
    setup_signup(tm, &stack, &builder);
    setup_login(tm, &stack, &builder);
    setup_connections(tm, &stack, &builder);
    setup_finish(app, tm, &window, &stack, &builder);

    let login_instead: gtk::Button = builder.object("login_instead").unwrap();
    login_instead.connect_clicked(clone!(
        @weak stack => move |_| stack.set_visible_child_name("login")
    ));

    let signup_instead: gtk::Button = builder.object("signup_instead").unwrap();
    signup_instead.connect_clicked(clone!(
        @weak stack => move |_| stack.set_visible_child_name("signup")
    ));

    window.set_application(Some(app));
    window.present();
}

fn go_back(stack: &adw::ViewStack) {
    match stack.visible_child_name().as_deref() {
        Some("signup" | "login") => {
            stack.set_visible_child_name("server_config");
        }
        Some("connections") => stack.set_visible_child_name("login"),
        Some("finish") => stack.set_visible_child_name("connections"),
        _ => println!("Back broken :( {:?}", stack.visible_child_name()),
    }
}
