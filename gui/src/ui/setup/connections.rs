use adw::prelude::*;
use common::{
    error::Error,
    tasks::{self, TaskMode},
};
use glib::{clone, MainContext};
use gtk::glib;

/// Sets up the connection page of the setup wizard.
pub fn setup_connections(
    tm: &TaskMode,
    stack: &adw::ViewStack,
    builder: &gtk::Builder,
) {
    let listbox: gtk::ListBox = builder
        .object("connection_list_box")
        .expect("Failed to get `connection_list_box`");

    let add: gtk::Button = builder
        .object("connection_add")
        .expect("Failed to get `connection_add`");

    let finish: gtk::Button = builder
        .object("connection_finish")
        .expect("Failed to get `connection_finish`");

    let error_label: gtk::Label = builder
        .object("connection_error_label")
        .expect("Failed to get `connection_error_label`");

    add.connect_clicked(clone!(
        @weak listbox => move |_| add_connection(&listbox);
    ));

    finish.connect_clicked(clone!(
        @weak stack,
        @strong tm,
        @weak error_label,
        @weak listbox, => move |finish| {
            let mc = MainContext::default();
            mc.spawn_local(clone!(
                @weak stack,
                @strong tm,
                @weak error_label,
                @weak listbox,
                @weak finish => async move {
                    finish.set_label("Finishing...");
                    finish.set_sensitive(false);
                    let _ = connect(
                        &tm,
                        &error_label,
                        &listbox,
                        &stack,
                    ).await;
                    finish.set_label("Finish");
                    finish.set_sensitive(true);

                }
            ));
        }
    ));
}

/// Adds a connection to a listbox
fn add_connection(listbox: &gtk::ListBox) {
    // Create a box to store our widgets in.
    let container = gtk::Box::builder().hexpand(true).spacing(0).build();

    container.prepend(
        &gtk::Entry::builder()
            .name("entry")
            .placeholder_text("username")
            .hexpand(true)
            .build(),
    );

    let button = gtk::Button::builder()
        .icon_name("edit-delete-symbolic")
        .css_classes(vec![String::from("circular")])
        .build();

    button.connect_clicked(clone!(
        @weak container, @weak listbox => move |_| {
            listbox.remove(&container.parent().unwrap());
        }
    ));

    container.append(&button);

    listbox.append(&container);
}

async fn connect(
    tm: &TaskMode,
    error_label: &gtk::Label,
    listbox: &gtk::ListBox,
    stack: &adw::ViewStack,
) -> Result<(), ()> {
    error_label.set_label("");
    // Get the connections frmo the listbox
    let conns = get_connections(listbox);

    let (Ok(_) | Err(_)) = add_connections(tm, &conns)
        .await
        .map_err(|e| error_label.set_text(&e.to_string()))
        .map(|_| stack.set_visible_child_name("finish"));
    Ok(())
}

fn get_connections(listbox: &gtk::ListBox) -> Vec<glib::GString> {
    // Loop through everything in the listbox
    let mut opt_child: Option<gtk::ListBoxRow> =
        listbox.first_child().map(|x| x.dynamic_cast().unwrap());
    let mut vec = Vec::new();

    // Iterate over the children
    while let Some(child) = opt_child {
        let entry: gtk::Entry = child
            .child()
            .unwrap()
            .first_child()
            .unwrap()
            .dynamic_cast()
            .unwrap();

        let text = entry.text();
        vec.push(text);

        // Go to the next child
        opt_child = child.next_sibling().map(|x| x.dynamic_cast().unwrap());
    }

    vec
}

async fn add_connections(
    tm: &TaskMode,
    conns: &[impl AsRef<str>],
) -> Result<(), Error> {
    // Delete the previous connections
    tasks::connections::remove_current_connections(tm).await?;

    // Loop through every connection and add them separately.
    //
    // This will exit at the first failed attempt.
    futures::future::try_join_all(conns.iter().map(|conn| {
        let tm = &tm;

        // Return the function which must be called
        tasks::add_connection(tm, conn.as_ref())
    }))
    .await?;

    Ok(())
}
