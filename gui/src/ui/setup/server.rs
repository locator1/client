use adw::prelude::*;
use common::{
    tasks::{self, TaskMode},
    Config,
};
use glib::{clone, MainContext};
use gtk::glib;

pub fn setup_server(
    tm: &TaskMode,
    stack: &adw::ViewStack,
    builder: &gtk::Builder,
) {
    // Gets a button to connect to the server
    let server_button: gtk::Button = builder
        .object("server_connect")
        .expect("Can't find the button to connect to the server. °-°");

    // Gets an entry containing the url
    let server_entry: gtk::Entry = builder
        .object("server_entry")
        .expect("Can't find server_entry.");

    // Gets an error label
    let server_error_label: gtk::Label = builder
        .object("server_error_label")
        .expect("Can't find server_error_label.");

    server_button.connect_clicked(clone!(
            @strong server_entry,
            @weak stack,
            @weak server_entry,
            @weak server_error_label,
            @strong tm => move |server_button| {
                let mc = MainContext::default();
                mc.spawn_local(clone!(
                    @strong server_entry,
                    @weak server_button,
                    @weak stack,
                    @weak server_entry,
                    @weak server_error_label,
                    @strong tm => async move {
                        connect_to_server(
                            &tm,
                            &server_entry,
                            &server_button,
                            stack,
                            server_error_label,
                        ).await;
                    }
                )
            );
        }
    ));

    server_entry.connect_activate(clone!(
            @weak server_button,
            @weak stack,
            @strong tm,
            @weak server_error_label => move |server_entry| {
                let mc = MainContext::default();
                mc.spawn_local(clone!(
                    @weak server_button,
                    @weak stack,
                    @strong server_entry,
                    @weak server_error_label,
                    @strong tm => async move {
                        connect_to_server(
                            &tm,
                            &server_entry,
                            &server_button,
                            stack,
                            server_error_label,
                        ).await;
                    }
                )
            );
        }
    ));
}

pub async fn connect_to_server(
    tm: &TaskMode,
    entry: &gtk::Entry,
    button: &gtk::Button,
    stack: adw::ViewStack,
    error_label: gtk::Label,
) {
    // Set the button to indicate the action
    button.set_label("Connecting...");

    let config = Config {
        server: Some(entry.text().to_string()),
        ..Config::default()
    };

    tasks::client_with_config(tm, config)
        .await
        .map_err(|x| error_label.set_label(&x.to_string()))
        .err();

    // Move to the next page
    stack.set_visible_child_name("signup");

    // Set the button back
    button.set_label("Connect");
}
