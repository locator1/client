use adw::prelude::*;
use common::tasks::{self, TaskMode};
use glib::{clone, MainContext};
use gtk::glib;

/// Sets up the final page of the setup wizard.
pub fn setup_finish(
    app: &gtk::Application,
    tm: &TaskMode,
    window: &adw::ApplicationWindow,
    _stack: &adw::ViewStack,
    builder: &gtk::Builder,
) {
    let finish: gtk::Button =
        builder.object("finish").expect("Failed to get `finish`");

    let error_label: gtk::Label = builder
        .object("finish_error_label")
        .expect("Faield to get `finish_error_label`");

    finish.connect_clicked(clone!(
         @weak app, @strong tm, @weak error_label, @weak window => move |finish| {
            let mc = MainContext::default();
            mc.spawn_local(clone!(
                @weak app, @strong tm, @weak finish, @weak error_label, @weak window => async move {
                    finish_setup(&app, &tm, &finish, &error_label, &window).await;
                }
            ));
        }
    ));
}

async fn finish_setup(
    _app: &gtk::Application,
    tm: &TaskMode,
    finish: &gtk::Button,
    error_label: &gtk::Label,
    window: &adw::ApplicationWindow,
) {
    finish.set_sensitive(false);
    finish.set_label("Finishing...");
    error_label.set_label("");

    match tasks::write_config(tm).await {
        Ok(_o) => {
            // Launch the main interface
            unimplemented!("Main ui is not implemented yet");
            // crate::ui::main::build_ui(app, client_outer).unwrap();
        }
        Err(e) => error_label.set_text(&e.to_string()),
    }

    finish.set_label("Finish");
    finish.set_sensitive(true);
    window.destroy();
}
