use adw::prelude::*;
use common::{
    error::Error,
    tasks::{self, TaskMode},
};
use glib::{clone, MainContext};
use gtk::glib;
use rand::rngs::OsRng;
use rsa::RsaPrivateKey;

pub fn setup_login(
    tm: &TaskMode,
    stack: &adw::ViewStack,
    builder: &gtk::Builder,
) {
    let username_entry: gtk::Entry = builder
        .object("login_username_entry")
        .expect("Can't find `login_username_entry`");

    let password_entry: gtk::PasswordEntry = builder
        .object("login_password_entry")
        .expect("Can't find `login_password_entry`");

    let error_label: gtk::Label = builder
        .object("login_error_label")
        .expect("Can't find `login_error_label`");

    let button: gtk::Button = builder
        .object("login_login")
        .expect("Can't find `signup_signup`");

    button.connect_clicked(clone!(
        @weak error_label, @weak username_entry, @weak password_entry, @strong tm, @weak stack => move |_| {
            let mc = MainContext::default();
            mc.spawn_local(clone!(
                 @weak stack, @weak error_label, @weak username_entry, @weak password_entry, @strong tm => async move {
                    login(stack, &tm, &error_label, &username_entry, &password_entry).await;
                 }
            ));
        }
    ));
}

/// logs in with the new account.
async fn login<P: EditableExt, U: EditableExt>(
    stack: adw::ViewStack,
    tm: &TaskMode,
    error_label: &gtk::Label,
    username_entry: &U,
    password_entry: &P,
) {
    error_label.set_label("");

    // Check if there is a username and password.
    if username_entry.text().is_empty() {
        error_label.set_label("No username.");
        return;
    }
    if password_entry.text().is_empty() {
        error_label.set_label("No password.");
        return;
    }

    let login = login_inner(
        tm,
        username_entry.text().to_string(),
        password_entry.text().to_string(),
    )
    .await;
    let (Ok(_) | Err(_)) = login
        .map_err(|e| error_label.set_text(&e.to_string()))
        .map(|_| stack.set_visible_child_name("connections"));
}

async fn login_inner(
    tm: &TaskMode,
    username: String,
    password: String,
) -> Result<(), Error> {
    let mut rng = OsRng;
    let bits: usize = 2048;
    let rsa_key = RsaPrivateKey::new(&mut rng, bits)?;

    // Set the RSA key.
    let pk = tasks::config::get_private_key(tm).await?;
    if pk.is_none() {
        tasks::config::mod_private_key(tm, Some(rsa_key)).await?;
    }

    // Set the username & password
    tasks::config::mod_username(tm, Some(username)).await?;
    tasks::config::mod_password(tm, Some(password)).await?;

    // Signup
    tasks::login(tm, true).await?;

    Ok(())
}
