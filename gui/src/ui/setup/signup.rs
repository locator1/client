use adw::prelude::*;
use common::{
    error::Error,
    tasks::{self, TaskMode},
};
use glib::{clone, MainContext};
use gtk::glib;
use rand::rngs::OsRng;
use rsa::RsaPrivateKey;

pub fn setup_signup(
    tm: &TaskMode,
    stack: &adw::ViewStack,
    builder: &gtk::Builder,
) {
    let username_entry: gtk::Entry = builder
        .object("signup_username_entry")
        .expect("Can't find `signup_username_entry`");

    let password_entry: gtk::PasswordEntry = builder
        .object("signup_password_entry")
        .expect("Can't find `signup_password_entry`");

    let password_entry_confirm: gtk::PasswordEntry = builder
        .object("signup_password_entry_confirm")
        .expect("Can't find `signup_password_entry_confirm`");

    let error_label: gtk::Label = builder
        .object("signup_error_label")
        .expect("Can't find `signup_error_label`");

    let button: gtk::Button = builder
        .object("signup_signup")
        .expect("Can't find `signup_signup`");

    password_entry.connect_changed(clone!(@weak error_label, @weak password_entry_confirm => move |entry|
        verify_matching_password_entries(&error_label, entry, &password_entry_confirm)
    ));

    password_entry_confirm.connect_changed(clone!(@weak error_label, @weak password_entry => move |confirm|
        verify_matching_password_entries(&error_label, confirm, &password_entry)
    ));

    button.connect_clicked(clone!(
        @weak stack, @weak error_label, @weak password_entry_confirm, @weak username_entry, @weak password_entry, @strong tm => move |_| {
            let mc = MainContext::default();
            mc.spawn_local(clone!(
                 @weak stack, @weak error_label, @weak password_entry_confirm, @weak username_entry, @weak password_entry, @strong tm => async move {
                    signup(stack, &tm, &error_label, &username_entry, &password_entry, &password_entry_confirm).await;
                 }
            ));
        }
    ));
}

/// Checks that two entries contain the same content. If they do not then
/// it writes an error to the label given.
fn verify_matching_password_entries<T: EditableExt, U: EditableExt>(
    error_label: &gtk::Label,
    entry1: &T,
    entry2: &U,
) {
    if entry1.text() == entry2.text() {
        error_label.set_label("");
    } else {
        error_label.set_label("Passwords do not match");
    }
}

/// Creates the account from the username entry and a password entry,
/// and a client, obviously.
///
/// Also logs in with the new account.
async fn signup<P: EditableExt, U: EditableExt, Pc: EditableExt>(
    stack: adw::ViewStack,
    tm: &TaskMode,
    error_label: &gtk::Label,
    username_entry: &U,
    password_entry: &P,
    password_entry_confirm: &Pc,
) {
    error_label.set_label("");

    // Handle some basic errors
    if password_entry.text() != password_entry_confirm.text() {
        error_label.set_label("Passwords do not match.");
        return;
    }
    if username_entry.text().is_empty() {
        error_label.set_label("No username.");
        return;
    }
    if password_entry.text().len() < 8 {
        error_label
            .set_label("Password too short (should be at least 8 characters).");
        return;
    }

    let signup = signup_inner(
        tm,
        username_entry.text().to_string(),
        password_entry.text().to_string(),
    )
    .await;
    let (Ok(_) | Err(_)) = signup
        .map_err(|e| error_label.set_text(&e.to_string()))
        .map(|_| stack.set_visible_child_name("connections"));

    stack.set_visible_child_name("connections");
}

async fn signup_inner(
    tm: &TaskMode,
    username: String,
    password: String,
) -> Result<(), Error> {
    let mut rng = OsRng;
    let bits: usize = 2048;
    let rsa_key = RsaPrivateKey::new(&mut rng, bits)?;

    // Set the RSA key.
    let pk = tasks::config::get_private_key(tm).await?;
    if pk.is_none() {
        tasks::config::mod_private_key(tm, Some(rsa_key)).await?;
    }

    // Set the username & password
    tasks::config::mod_username(tm, Some(username)).await?;
    tasks::config::mod_password(tm, Some(password)).await?;

    // Signup
    tasks::signup(tm).await?;

    Ok(())
}
