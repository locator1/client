use std::fmt::Display;

use adw::prelude::*;

use crate::util::window_error;

/// A trait to allow one to log an error directly to a gtk label
pub trait LogErrToLabel {
    type Output;

    fn log_to_label(self, label: &gtk::Label) -> Self::Output;
    fn log_to_dialog<P: IsA<gtk::Application>>(
        self,
        app: Option<&P>,
        header: &str,
    ) -> Self::Output;
    fn log_to_dialog_with_parent<
        P: IsA<gtk::Application>,
        W: IsA<gtk::Window>,
    >(
        self,
        app: Option<&P>,
        header: &str,
        parent: &W,
    ) -> Self::Output;
}

impl<T, E: Display> LogErrToLabel for Result<T, E> {
    type Output = Option<T>;

    fn log_to_label(self, label: &gtk::Label) -> Self::Output {
        match self {
            Ok(o) => Some(o),
            Err(e) => {
                label.set_text(&e.to_string());
                None
            }
        }
    }

    fn log_to_dialog<P: IsA<gtk::Application>>(
        self,
        app: Option<&P>,
        header: &str,
    ) -> Self::Output {
        match self {
            Ok(o) => Some(o),
            Err(e) => {
                let dialog = window_error(header, &e.to_string());

                // Show it
                dialog.set_application(app);
                dialog.present();
                None
            }
        }
    }

    fn log_to_dialog_with_parent<
        P: IsA<gtk::Application>,
        W: IsA<gtk::Window>,
    >(
        self,
        app: Option<&P>,
        header: &str,
        parent: &W,
    ) -> Self::Output {
        match self {
            Ok(o) => Some(o),
            Err(e) => {
                let dialog = window_error(header, &e.to_string());

                // Show it
                dialog.set_application(app);
                dialog.set_transient_for(Some(parent));
                dialog.present();
                None
            }
        }
    }
}
