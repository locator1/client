macro_rules! resource {
    ($literal:literal) => {
        include_str!(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/data/resources/",
            $literal
        ))
    };
}
pub(crate) use resource;
