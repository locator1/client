use adw::prelude::*;

#[must_use = "Window must be presented"]
pub fn window_error(
    primary_err: &str,
    secondary_err: &str,
) -> gtk::MessageDialog {
    let dialog = gtk::MessageDialog::builder()
        .title(primary_err)
        .text(primary_err)
        .secondary_text(secondary_err)
        .buttons(gtk::ButtonsType::Ok)
        .message_type(gtk::MessageType::Error)
        .build();

    dialog.connect_response(|win, _response| win.close());

    dialog
}

/// Propogates an error about there being no server.
pub fn no_server_error<P: IsA<gtk::Application>>(app: Option<&P>) {
    let dialog = window_error("Error in configuration", "No server");
    dialog.set_application(app);
    dialog.present();
}
