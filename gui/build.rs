use std::error::Error;
use std::fs;
use std::path::PathBuf;

macro_rules! compile_ui {
    ($path:literal) => {
        println!("Compiling ui: {}", $path);
        let interface: gtk_comfy::Interface =
            serde_yaml::from_str(include_str!($path))?;
        fs::write(
            {
                let mut path = PathBuf::from($path);
                path.set_extension("xml");
                path
            },
            interface.to_string(),
        )?;
    };
}

pub fn main() -> Result<(), Box<dyn Error>> {
    compile_ui()?;
    compile_resources()?;

    Ok(())
}

fn compile_ui() -> Result<(), Box<dyn Error>> {
    compile_ui!("data/resources/ui/setup.ui.yaml");
    compile_ui!("data/resources/ui/main_interface.ui.yaml");
    compile_ui!("data/resources/ui/add_connection.ui.yaml");
    compile_ui!("data/resources/ui/incoming.ui.yaml");
    compile_ui!("data/resources/ui/outgoing.ui.yaml");

    Ok(())
}

fn compile_resources() -> Result<(), Box<dyn Error>> {
    gio::compile_resources(
        "data/resources/",
        "data/resources/resources.gresource.xml",
        "compiled.gresource",
    );

    Ok(())
}
