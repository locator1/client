use std::env;
use std::path::PathBuf;

use thiserror::Error;
use tokio::fs;

#[derive(Debug, Error)]
pub enum Error {
    #[error(
        "not sure where to install, make sure either \
        `$HOME` or $XDG_DATA_HOME` is set"
    )]
    InstallPathUnkown,
    #[error("io error: {0}")]
    Io(#[from] tokio::io::Error),
}

pub async fn install(secs: usize) -> Result<(), Error> {
    log::info!("Formatting timer...");
    let timer = format!(include_str!("../../data/locator.timer"), secs);

    log::info!("Creating systemd directory...");

    // Gets the path of `~/.local/share/systemd/user`
    let mut path = env::var("XDG_DATA_HOME")
        .ok()
        .map(PathBuf::from)
        .map(Ok)
        .unwrap_or_else(|| {
            if let Ok(home) = env::var("HOME") {
                let mut path: PathBuf = home.into();
                path.push(".local");
                path.push("share");
                Ok(path)
            } else {
                Err(Error::InstallPathUnkown)
            }
        })?;
    path.push("systemd");
    path.push("user");

    fs::create_dir_all(&path).await?;

    log::info!("Copying files...");
    tokio::try_join!(
        fs::write(path.join("locator.timer"), &timer),
        fs::write(
            path.join("locator.service"),
            include_str!("../../data/locator.service")
        ),
    )?;

    log::info!("Done!");

    println!(
        "Completed successfuly.\n\
        Now you should be able to enable `locator.timer` \
        by running:\n\
        \n    \
        systemctl --user enable locator.timer"
    );

    Ok(())
}
