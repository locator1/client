use common::Config;
use common::tasks::{self, TaskMode};
// use common::tasks::{login, signup};
use rand::rngs::OsRng;
use rpassword::read_password_from_tty as read_pass;
use rsa::pkcs8::FromPrivateKey;
use rsa::RsaPrivateKey;
use tokio::fs;
use yansi::Paint;

// #[derive(Error, Debug)]
// pub enum Error {
//     #[error("io error: {0}")]
//     IoError(#[from] std::io::Error),
//     #[error("decoding rsa error: {0}")]
//     DecodeRsa(#[from] rsa::pkcs8::Error),
//     #[error("rsa error: {0}")]
//     Rsa(#[from] rsa::errors::Error),
//     #[error("signup error: {0}")]
//     SignupError(#[from] common::tasks::signup::Error),
//     #[error("login error: {0}")]
//     LoginError(#[from] common::tasks::login::Error),
//     #[error("error serialising to yaml: {0}")]
//     Yaml(#[from] serde_yaml::Error),
//     #[error("error talking to daemon: {0}")]
//     Daemon(#[from] common::streams::Error),
//     #[error("error creating client: {0}")]
//     WithConfigError(#[from]
// common::tasks::acquire_client::WithConfigError), }

/// Creates a new configuration and user.
///
/// This will also create a client in the process because it dosen't
/// make sense to reuse an existing one.
pub async fn create(
    task_mode: &mut TaskMode,
    config: crate::CreateConfig,
) -> Result<(), common::error::Error> {
    let server = config
        .server
        .map(Ok)
        .unwrap_or_else(|| uquery::string("Enter a server:"))?;

    let nominatim = config.nominatim.map(Ok).unwrap_or_else(|| {
        uquery::string_with_default(
            "Enter a nominatim instance:",
            Some("https://nominatim.openstreetmap.org/"),
        )
    })?;

    let do_login: bool = config.login.map(Ok).unwrap_or_else(|| {
        uquery::boolean("Do you already have an account?", Some(false))
    })?;

    let username = config.username.map(Ok).unwrap_or_else(|| {
        uquery::string("Enter a username for the account:")
    })?;

    let password = config.password.map(Ok).unwrap_or_else(|| {
        read_pass(Some(&format!(
            "{} Password for {}: ",
            Paint::red("::").bold(),
            username
        )))
    })?;

    let rsa_key;
    if let Some(key_path) = config.rsa_key {
        let key = fs::read_to_string(key_path).await?;
        rsa_key = RsaPrivateKey::from_pkcs8_pem(&key)?;
    } else {
        let mut rng = OsRng;
        let bits: usize = uquery::parsable_with_default(
            "How many bits for your RSA key?",
            2048,
        )?;
        rsa_key = RsaPrivateKey::new(&mut rng, bits)?;
    }

    let config = Config {
        nominatim: Some(nominatim),
        server: Some(server),
        username: Some(username),
        password: Some(password),
        private_key: Some(rsa_key),
        incoming_duration: None,
        outgoing_duration: None,
    };

    log::info!("Clearing client...");
    tasks::clear_client(task_mode).await?;

    log::info!("Creating client with configuration...");
    tasks::client_with_config(task_mode, config).await?;

    if do_login {
        log::info!("Logging in...");
        tasks::login(task_mode, true).await?;
        log::info!("Logged in.");
    } else {
        log::info!("Signing up...");
        tasks::signup(task_mode).await?;
        log::info!("Signed up.");
    }

    log::info!("Writing config file...");
    tasks::write_config(task_mode).await?;

    Ok(())
}
