use std::fmt::Debug;
use std::fmt::Display;
use std::io::{self, Write};
use std::os::unix::net::UnixStream;

use serde::Serialize;

pub trait LogAndQuit {
    type Output;

    fn log_and_quit(self) -> Self::Output;
    fn log_and_ignore(self);
}

impl<T, E: Display> LogAndQuit for Result<T, E> {
    type Output = T;

    fn log_and_quit(self) -> Self::Output {
        match self {
            Ok(o) => o,
            Err(e) => {
                log::error!("{}", e);
                std::process::exit(1)
            }
        }
    }

    fn log_and_ignore(self) {
        match self {
            Ok(_o) => (),
            Err(e) => {
                log::error!("{}", e);
            }
        }
    }
}

pub trait SendToStream {
    fn send_to_stream(&self, stream: &mut UnixStream) -> io::Result<()>;
}

impl<T: Serialize + Debug, E: Serialize + Debug> SendToStream for Result<T, E> {
    fn send_to_stream(&self, stream: &mut UnixStream) -> io::Result<()> {
        let serialised = serde_json::to_vec(self)?;
        log::trace!("Sending to stream...");
        stream.write_all(&serialised)?;
        stream.flush()
    }
}
