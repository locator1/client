#![deny(unused_must_use)]

extern crate clap;
use common::tasks::TaskMode;
pub mod cfg;
pub mod connections;
pub mod daemon;
pub mod install;
// pub mod send;
pub mod util;
use std::path::PathBuf;

use clap::{Args, Parser, Subcommand};
pub use common::ClientOuter;
use util::LogAndQuit;

pub static ID: &str = "org.john_t.Locator";

#[derive(Parser)]
#[clap(about, version, author)]
struct Opts {
    #[clap(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
enum Command {
    /// Installs a user based systemd timer.
    ///
    /// Will not wake on suspend.
    Install { time: usize },
    /// Creates a configuration
    #[clap(name = "cfg")]
    CreateConfig(CreateConfig),
    /// Creates a daemon process
    Daemon,
    /// Shuts down any daemon processes
    DaemonShutdown,
    /// Commands to do with connection(s)
    #[clap(name = "conn")]
    #[clap(subcommand)]
    Connection(ConnectionSubCmd),
    /// Logs the user out and in the process deletes your config.
    Logout,
}

#[derive(Subcommand)]
pub enum ConnectionSubCmd {
    /// Add connection(s)
    Add(ConnectionAdd),
    /// Remove connection(s)
    Remove(ConnectionRemove),
    /// Lists all your connection(s)
    List(ConnectionsList),
    /// Send the GPS information to some connections
    Send(ConnectionsSend),
}

#[derive(Args)]
pub struct ConnectionsList {
    #[clap(long, short)]
    gps: bool,
}

#[derive(Args)]
pub struct ConnectionAdd {
    connections: Vec<String>,
}

#[derive(Args)]
pub struct ConnectionRemove {
    /// Senders to remove (this implies you are the reciever.)
    #[clap(short, long = "sender")]
    pub senders: Vec<String>,
    /// Recievers to remove (this implies you are the sender.)
    #[clap(short, long = "receiver")]
    pub recievers: Vec<String>,
}

#[derive(Args)]
pub struct ConnectionsSend {
    /// Sends information to all the connections
    #[clap(short, long)]
    pub all: bool,
    /// Recievers to send to.
    #[clap(short, long = "receiver")]
    pub recievers: Vec<String>,
    /// Accuracy (in meters) that is acceptable.
    #[clap(short = 'c', long = "accuracy")]
    pub accuracy: Option<f64>,
    /// Max timeout for the location in seconds. (Default: 30)
    #[clap(short, long)]
    pub timeout: Option<u64>,
}

#[derive(Args)]
pub struct CreateConfig {
    /// Log in rather than sign up
    #[clap(long, short)]
    login: Option<bool>,

    /// The username of the user to sign up / log in
    #[clap(long, short)]
    username: Option<String>,

    /// The password of the user (recommended to enter in stdin)
    #[clap(long, short)]
    password: Option<String>,

    /// The locator server.
    #[clap(long, short)]
    server: Option<String>,

    /// The nominatim instance.
    #[clap(long, short)]
    nominatim: Option<String>,

    /// A path to an rsa key.
    #[clap(long = "rsa", short = 'k')]
    rsa_key: Option<PathBuf>,
}

#[tokio::main]
async fn main() {
    env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or("info"),
    )
    .init();

    // Run clap to find the arguments.
    let clap = Opts::parse();

    match clap.command {
        Command::Install { time } => {
            install::install(time).await.log_and_quit();
        }
        Command::CreateConfig(c) => {
            let mut task_mode = TaskMode::new().await;
            cfg::create(&mut task_mode, c).await.log_and_quit();
        }
        Command::Daemon => daemon::run().await,
        Command::DaemonShutdown => {
            common::tasks::shutdown().await.log_and_quit()
        }
        Command::Logout => {
            let tm = TaskMode::new().await;
            common::tasks::config::logout(&tm).await.log_and_quit()
        }
        Command::Connection(cmd) => connections::handle(cmd).await,
    }
}
