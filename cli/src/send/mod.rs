use std::sync::{
    atomic::{AtomicI8, Ordering},
    Arc,
};
use std::time::Duration;

use ::api::apis::default_api as api;
use ::api::apis::Error as ApiError;
use ::api::models;
use chrono::prelude::*;
use common::rsa_key_cache::RsaKeyCacheOuter;
use geoclue::prelude::*;
use glib::{clone, MainContext};
pub use common::tasks::acquire_client;
pub use common::tasks::get;
use models::ListConnectionsItem;
use rand::rngs::OsRng;
use rsa::PaddingScheme;
use rsa::PublicKey;
use thiserror::Error;
use tokio::sync::Mutex;
use tokio::time::sleep;

use crate::util::LogAndQuit;
use crate::ClientOuter;

pub async fn run() {
    let c = glib::MainContext::default();
    let l = glib::MainLoop::new(Some(&c), false);

    c.push_thread_default();
    c.spawn_local(clone!(
        @strong l => async move {
            run_main(l).await;
        }
    ));

    l.run();
}

async fn run_main(l: glib::MainLoop) {
    log::info!("Starting up!");

    log::info!("Creating a key cache...");
    let key_cache = Default::default();
    log::info!("Created a key cache.");

    // Get the client
    log::info!("Getting client...");
    let client = acquire_client::acquire_client().await.log_and_quit();
    log::info!("Got client.");

    // Get all the connections
    log::info!("Getting connections...");
    let connections = get::get_connections(&client).await.log_and_quit();
    let connections = Arc::new(Mutex::new(connections));
    log::info!("Got connections.");

    // Send stuff to the server
    log::info!("Sending gps to the server...");
    send_location(&l, &client, connections, &key_cache)
        .await
        .log_and_quit();

    log::info!("Done!");
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("error connecting to dbus via geoclue: {0}")]
    GeoClue(#[from] glib::Error),
    #[error("error joining tokio tasks: {0}")]
    JoinError(#[from] tokio::task::JoinError),
    #[error("error with rsa keys: {0}")]
    Rsa(#[from] rsa::errors::Error),
    #[error("error making request from server: {0}")]
    Api(#[from] ApiError<api::GpsByRecieverRecieverPutError>),
    #[error("getting rsa key failed: {0}")]
    GetRsaKey(#[from] common::rsa_key_cache::GetRsaKeyError),
    #[error("getting username failed: {0}")]
    ConfigError(#[from] common::config::ConfigError),
}

pub async fn send_location(
    l: &glib::MainLoop,
    client: &ClientOuter,
    locs: Arc<Mutex<Vec<ListConnectionsItem>>>,
    rsa_key_cache: &RsaKeyCacheOuter,
) -> Result<(), Error> {
    // Create a simple geoclue interface
    log::debug!("Getting Geoclue Simple...");
    let simple =
        geoclue::Simple::new_future(crate::ID, geoclue::AccuracyLevel::Exact)
            .await?;
    log::debug!("Got Geoclue Simple.");

    let mc = MainContext::default();

    // We have a buffer amount of requests because for some
    // odd reason it seems geoclue gives a less accurate
    // result on the first reading. Not sure why but we will
    // set this buffer so that per request it must send two requests.
    let buffer = Arc::new(AtomicI8::new(3));

    on_location_get(&simple, l, &locs, client, rsa_key_cache, &buffer).await;

    simple.connect_location_notify(clone!(
        @strong client,
        @strong locs,
        @strong l,
        @strong buffer,
        @strong rsa_key_cache =>
            move |simple| {
                mc.spawn_local(clone!{
                    @weak simple,
                    @strong l,
                    @strong locs,
                    @strong client,
                    @strong rsa_key_cache,
                    @strong buffer => async move {
                        on_location_get(&simple, &l, &locs, &client, &rsa_key_cache, &buffer).await;
                    }
                })
        }
    ));

    for _ in 0..3 {
        sleep(Duration::from_secs(6)).await;
        log::debug!(
            "Regathering location despite no \
            new information found (probably on a computer.)"
        );
        on_location_get(&simple, l, &locs, client, rsa_key_cache, &buffer)
            .await;
    }

    Ok(())
}

pub async fn on_location_get(
    simple: &geoclue::Simple,
    l: &glib::MainLoop,
    locs: &Arc<Mutex<Vec<ListConnectionsItem>>>,
    client: &ClientOuter,
    rsa_key_cache: &RsaKeyCacheOuter,
    buffer: &AtomicI8,
) {
    log::trace!("Unlocking `locs` mutex...");
    let locs = locs.lock().await;
    log::trace!("Unlocked `locs` mutex.");

    // Gets the location
    log::debug!("Getting location...");
    let location = simple.location();
    log::trace!(
        "Location is: lat:{},long:{}",
        location.latitude(),
        location.longitude(),
    );

    let buffer_local = buffer.load(Ordering::Relaxed);
    buffer.store(buffer_local - 1, Ordering::Relaxed);

    if buffer_local <= 1 {
        for loc in &*locs {
            send_gps_information(
                client,
                &loc.reciever,
                rsa_key_cache,
                &location,
            )
            .await
            .unwrap();
        }
        l.quit();
    }
}

/// Sends all the gps information to all of the connections who so need it.
pub async fn send_gps_information(
    client: &ClientOuter,
    connection: &str,
    rsa_key_cache: &RsaKeyCacheOuter,
    location: &impl LocationExt,
) -> Result<(), Error> {
    // Get all the values from the mutexes
    let client = client.lock().await;
    let mut key_cache = rsa_key_cache.lock().await;

    if connection == client.config.username()? {
        return Ok(());
    }
    log::debug!("Sending gps information to {}...", connection);

    // Get the rand
    let mut rng = OsRng;
    let latitude = location.latitude().to_be_bytes();
    let longitude = location.longitude().to_be_bytes();

    // Get the key
    //
    // We have to own it because of the closure.
    //
    // TODO: Find a better solution for this.
    log::debug!("Get key...");
    let key = key_cache.key(connection, &client).await?.clone();

    // Encrypt the data, done as a task as it can be CPU heavy and
    // could block.
    log::debug!("Encrypt data...");
    let (longitude, latitude) = tokio::task::spawn_blocking(
        move || -> Result<(Vec<u8>, Vec<u8>), Error> {
            // Get the padding schemes
            let lat_padding = PaddingScheme::new_pkcs1v15_encrypt();
            let long_padding = PaddingScheme::new_pkcs1v15_encrypt();

            // Encrypt it
            let longitude =
                key.encrypt(&mut rng, long_padding, &longitude[..])?;
            let latitude = key.encrypt(&mut rng, lat_padding, &latitude[..])?;

            Ok((longitude, latitude))
        },
    )
    .await??;

    // Send it to the server
    log::debug!("Sending data...");
    api::gps_by_reciever_reciever_put(
        &client.client,
        connection,
        client.key.as_ref().unwrap(),
        Some(models::Gps {
            longitude: Some(longitude),
            latitude: Some(latitude),
            date: Some(Utc::now().to_rfc3339()),
        }),
    )
    .await?;

    Ok(())
}
