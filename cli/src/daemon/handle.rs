use std::os::unix::net::UnixStream;

pub use common::streams::Msg;
pub use common::tasks::{self, Task, TaskMode};

use crate::util::{LogAndQuit, SendToStream};

macro_rules! task_handle {
    ($tm:ident, $stream:ident, $func:path, { $( $y:ident ),* } ) => {
        {
            let mut task_mode = TaskMode::clone($tm);
            tokio::spawn(async move {
                $func(&mut task_mode, $($y, )*)
                    .await
                    .send_to_stream(&mut $stream)
                    .log_and_ignore();
            });
            false
        }
    }
}

/// Handles a given message. Returns `true` if the application should
/// quit.
pub async fn handle(
    msg: Msg<Task>,
    task_mode: &mut TaskMode,
    mut stream: UnixStream,
) -> bool {
    log::debug!("Handling: {:?}", msg);
    match msg.msg {
        Task::Shutdown => true,
        Task::Noop => false,
        Task::ModConfig { mask, config } => {
            task_handle!( task_mode, stream, tasks::mod_config, { mask, config } )
        }
        Task::ClearClient => {
            task_handle!(task_mode, stream, tasks::clear_client, {})
        }
        Task::ClientWithConfig(config) => {
            task_handle!(task_mode, stream, tasks::client_with_config, {
                config
            })
        }
        Task::AcquireClient => {
            task_handle!(task_mode, stream, tasks::acquire_client, {})
        }
        Task::Signup => {
            task_handle!(task_mode, stream, tasks::signup, {})
        }
        Task::Login(new_key) => {
            task_handle!(task_mode, stream, tasks::login, {new_key})
        }
        Task::WriteConfig => {
            task_handle!(task_mode, stream, tasks::write_config, {})
        }
        Task::AddConnection(connection) => {
            task_handle!(task_mode, stream, tasks::add_connection, {
                connection
            })
        }
        Task::RemoveConnectionFromSender(connection) => {
            task_handle!(
                task_mode,
                stream,
                tasks::remove_connection_from_sender,
                { connection }
            )
        }
        Task::RemoveConnectionFromReceiver(connection) => {
            task_handle!(
                task_mode,
                stream,
                tasks::remove_connection_from_reciever,
                { connection }
            )
        }
        Task::ListConnections => {
            task_handle!(task_mode, stream, tasks::list_connections, {})
        }
        Task::GpsGet(connection) => {
            task_handle!(task_mode, stream, tasks::gps::get, { connection })
        }
        Task::SendLocation { gps, user } => {
            task_handle!(task_mode, stream, tasks::gps::send, {gps, user})
        }
        Task::GetConfig(mask) => {
            task_handle!(task_mode, stream, tasks::config::get, { mask })
        }
        Task::GetKey => {
            task_handle!(task_mode, stream, tasks::misc::get_key, {})
        }
        Task::NominatimReverseGeocode {longitude, latitude} => {
            task_handle!(task_mode, stream, tasks::nominatim::reverse_geocode, {longitude, latitude})
        }
        Task::HasKey => {
            task_handle!(task_mode, stream, tasks::misc::has_key, {})
        }
        Task::Logout => {
            task_handle!(task_mode, stream, tasks::config::logout, {})
        }
    }
}
