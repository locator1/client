use std::env;
use std::io::Read;
use std::os::unix::net::UnixListener;
use std::path::PathBuf;

use common::rsa_key_cache::RsaKeyCacheOuter;
use common::streams::Msg;
use common::tasks::TaskMode;
pub use common::tasks::{get, Task};
use tokio::fs;

use crate::util::LogAndQuit;
mod handle;

pub async fn run() {
    run_main().await;
}

async fn run_main() {
    log::info!("Starting up!");

    log::info!("Creating a key cache...");
    let _key_cache = RsaKeyCacheOuter::default();
    log::info!("Created a key cache.");

    // Get the client
    log::info!("Getting task mode...");
    let mut task_mode = TaskMode::local().await;
    log::info!("Got task mode.");

    // Get all the connections
    log::info!("Getting connections...");
    // let connections = get::get_connections(&client).await.log_and_quit();
    // let _connections = Arc::new(Mutex::new(connections));
    log::info!("Got connections.");

    let mut path: PathBuf = env::var("XDG_RUNTIME_DIR")
        .log_and_quit()
        .parse()
        .log_and_quit();
    path.push("locator.sock");

    log::info!("Opening socket at {} ...", path.to_string_lossy());
    let listener = UnixListener::bind(&path).log_and_quit();

    loop {
        let (mut stream, addr) = listener.accept().log_and_quit();
        log::trace!("Address: {:?}", addr);
        let mut vec = Vec::new();
        match stream.read_to_end(&mut vec) {
            Ok(_o) => match serde_json::from_slice(&vec) {
                Ok(o) => {
                    let msg: Msg<Task> = o;
                    if handle::handle(msg, &mut task_mode, stream).await {
                        break;
                    }
                }
                Err(e) => {
                    log::error!("error serialising message: {}", e);
                    log::warn!(
                        "the string was: {}",
                        String::from_utf8_lossy(&vec)
                    );
                    stream.shutdown(std::net::Shutdown::Write).log_and_quit();
                }
            },
            Err(e) => {
                log::error!("error reading from stream: {}", e);
            }
        }
        log::trace!("Finished loop...");
    }
    fs::remove_file(path).await.log_and_quit();
    log::info!("Done!");
}
