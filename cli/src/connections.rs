use common::DecryptedGps;
use tokio::time::{sleep, Duration};
use geoclue::{ManagerProxy, LocationProxy};
use common::tasks;
use tasks::{TaskMode, config::ConfigMask };
use serde::{Deserialize, Serialize};
use futures_util::stream::StreamExt;

use crate::util::LogAndQuit;
use crate::{
    ConnectionAdd, ConnectionRemove, ConnectionSubCmd, ConnectionsList, ConnectionsSend,
};

pub async fn handle(cmd: ConnectionSubCmd) {
    let mut task_mode = TaskMode::new().await;
    log::info!("Got task mode.");
    match cmd {
        ConnectionSubCmd::Add(info) => add(&mut task_mode, info).await,
        ConnectionSubCmd::Remove(info) => remove(&mut task_mode, info).await,
        ConnectionSubCmd::List(info) => list(&mut task_mode, info).await,
        ConnectionSubCmd::Send(info) => send(&mut task_mode, info).await.log_and_quit(),
    }
}

async fn add(task_mode: &mut TaskMode, add: ConnectionAdd) {
    for add in add.connections {
        tasks::add_connection(task_mode, add).await.log_and_quit();
    }
}

async fn remove(task_mode: &mut TaskMode, remove: ConnectionRemove) {
    // Check the user actually entered seomthing
    if remove.senders.is_empty() && remove.recievers.is_empty() {
        uquery::warning(
            "No senders or receivers given",
            Some("Use the -r and -s options to specify some senders and receivers")
        );
    }
    
    // Remove all the senders
    for remove in remove.senders {
        tasks::remove_connection_from_sender(task_mode, remove)
            .await
            .log_and_quit();
    }

    // Remove all the receivers
    for remove in remove.recievers {
        tasks::remove_connection_from_reciever(task_mode, remove)
            .await
            .log_and_quit();
    }
}

#[derive(Serialize, Deserialize)]
pub struct ConnectionsListWithInfo {
    pub sender: String,
    pub receiver: String,
    pub gps: Option<DecryptedGps>,
}

async fn list(task_mode: &mut TaskMode, info: ConnectionsList) {
    let conns_raw = tasks::list_connections(task_mode).await.log_and_quit();
    let mut conns = Vec::new();
    let username_cfg = tasks::config::get(task_mode, ConfigMask::USERNAME).await.log_and_quit();
    for conn in conns_raw {
        conns.push(ConnectionsListWithInfo {
            gps: if info.gps {
                if Some(&conn.sender) != username_cfg.username.as_ref() {
                    Some(
                        tasks::gps::get(task_mode, &conn.sender)
                            .await
                            .log_and_quit(),
                    )
                } else {
                    None
                }
            } else {
                None
            },
            receiver: conn.reciever,
            sender: conn.sender,
        });
    }
    println!("{}", serde_json::to_string(&conns).log_and_quit());
}

async fn send(task_mode: &mut TaskMode, info: ConnectionsSend) -> Result<(), common::error::Error> {
    // Check the user actually entered seomthing
    if info.recievers.is_empty() && !info.all {
        uquery::warning(
            "No senders or receivers given",
            Some("Use the -r and -a options to specify some receivers")
        );
        return Ok(());
    }

    // Work out who we need to send to.
    let mut receivers;

    if info.all {
        let connections = tasks::list_connections(task_mode).await?;
        let username_cfg = tasks::config::get(task_mode, ConfigMask::USERNAME).await?;

        receivers = Vec::new();
        for conn in connections {
            if Some(&conn.sender) == username_cfg.username.as_ref() {
                receivers.push(conn.reciever);
            }
        }
    } else {
        receivers = info.recievers;
    }

    // Create a geoclue client
    let conn = zbus::Connection::system().await?;
    let manager = ManagerProxy::new(&conn).await?;
    let client = manager.get_client().await?;

    // Configure the client
    client.set_desktop_id(common::ID).await?;
    client.set_requested_accuracy_level(
        geoclue::AccuracyLevel::Exact
    ).await?;

    // Start waiting for changes in the location
    let mut location_updated = client.receive_location_updated().await?;
    client.start().await?;

    // Make sure that we don't take longer than timeout.
    tokio::select! {
        _ = sleep(Duration::from_secs(info.timeout.unwrap_or(30))) => {
            log::warn!("Timeout reached before an accurate reading was got!");
        }
        res = async {
            // Loop through the location
            while let Some(signal) = location_updated.next().await {
                // Get the args to create a location
                let args = signal.args()?;

                log::info!("Getting location...");

                // Create the location
                let location = LocationProxy::builder(&conn)
                    .path(args.new())?
                    .build()
                    .await?;

                log::info!("Got location.");

                log::info!("Getting location properties...");
                let (longitude, latitude, accuracy) = tokio::try_join!(
                    location.longitude(),
                    location.latitude(),
                    location.accuracy(),
                )?;

                log::info!("Got location properties.");

                // Check if the accuracy specified is okay.
                if accuracy >= info.accuracy.unwrap_or(f64::INFINITY) {
                    log::info!("Retrying because of inaccuracy...");
                    continue;
                }

                log::info!("Looping through receivers...");
                // Send the location.
                for receiver in &receivers {
                    log::debug!("Sending to {}...", receiver);
                    tasks::gps::send(task_mode, DecryptedGps {
                        longitude: Some(longitude),
                        latitude: Some(latitude),
                        accuracy: Some(accuracy),
                        date: None,
                    }, receiver).await?;
                }
                log::info!("Send to all receivers.");
                break;
            }
            Ok::<(), common::error::Error>(())
        } => res?
    }

    Ok(())
}
