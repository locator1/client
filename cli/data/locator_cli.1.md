---
title: LOCATOR_CLI
section: 1
header: locator cli
footer: locator_cli 0.1.0
date: January 29, 2022
---

# NAME

locator_cli - A program for interacting with the locator service

# SYNPOSIS

**locator_cli** <*command*> <*args*>

# DESCRPTION

**locator_cli** is a program for interacting with locator, a service
to share location with family and friends. It can make an account, login
to an existing one, and send and receive gps information.

# OPTIONS

## Install

**lcator_cli install**

Installs a local systemd timer to the user's systemd location.
This timer **WILL NOT** wake the system from sleep

## Create Config

```sh
locator_cli cfg <args>
```

Any of the following flags can be ommitted:

`-l`, `--login`
: Log in rather than sign up

`-u`, `--username`
: The username of the user to sign up / log in.

`-p`, `--password`.
: The password of the user (recommended not to include because of shell
: history.

`-s`, `--server`
: The locator server to use (beginning with `https://`).

`-n`, `--nominatim`
: Name of the nominatim instance to use.

`-k`, `--rsa`
: The path to an rsa key to use.

The rest will be entered through the stdin.

## Connections

Connections are the heart of locator. A connection has a receiver, who gets
the location, and a sender, who give it.

Here are some subcommands to do with connections:

### Add

Lets you add outgoing connections.

```
locator_cli conn add <args>
```

Where `<args>` is a list of people to add.

### Remove

Lets you remove connections, incoming or outgoing.

```
locator_cli conn remove [-s][-r]
```

`-s`, `--sender`
: Username of sender to remove the connection.

`-r`, `--receiver`
: Username of receiver to remove the connection.

These arguments can be specified any number of times.

### List

Lists all of ones connections, in json.

```
locator_cli conn list [-g]
```

`-g`, `--gps`
: Wether or not to include GPS information in the output.

#### Format

The output will be in the following format (JSON):

```json
[
     {
        "sender": "<sender>",
        "receiver": "<receiver">,
        "gps": {
            "longitude": 0.0,
            "latitude": 0.0,
            "timestamp": "yyyy-mm-ddThh:MM::ss.m+tt:tt",
            "accuracy": null,
        }
     }
]
```

Gps will only be included if the option is specified:

 * `timetamp` is an iso8601 date.
 * `longitude` and `latitude` are floats
 * `accuracy` is a float, but will (for now) always be
   null.

### Send

Sends the gps information to one or more users.

Usage:

```
locator_cli conn send [-a|-r] [-c <accuracy>] [-t <timeout>]
```

`-a`, `--all`
: Send to all receivers.

`-r`, `--receiver`
: Sends to this specific receiver.

`-c`, `--accuracy`
: Sets a minimum accuracy for GeoClue that will be accepted (in meters).

On a pinephone you can get 10 meters of accuracy so I
would set it to 12 just to be sure. You can check the
accuracy which you get by using the following command:

```
/usr/lib/geoclue-2.0/demos/where-am-i
```

which will, among other things, print out the accuracy.

If not specified accuracy will not be considered.

`-t`, `--timeout`
: Maximum timout in seconds.

This is only useful with the `-c`, `--accuracy` option,
unless you have *reall* slow wifi.

# EXAMPLES

## Create an account:

This is how you would create an account for the first time.

```
locator_cli cfg
```

And then answer the following prompts:

`:: Enter a server:` *https://example.org* ↵  
`:: Enter a nominatim instance (Defaults to: https://nominatim.openstreetmap.org/)` ↵  
`:: Do you already have an account? [y/N]` ↵  
`:: Enter a username for the account:` *johndoe* ↵  
`:: Password for johndoe:` *pass* ↵  
`:: How many bits for you rsa key? (Defaults to: 2048)` ↵  

## Adds Connections

Adds a connection

```
locator_cli conn add anneparker
```

## Send information

```
locator_cli conn send -a
```

## Receive connections

Here we receive all the connections (`conn send`) and the gps
information (`-g`) and pipe into `jq` for pretty printing.

```
locator_cli conn list -g | jq .
```

## Cron Job

This is the cron job which I use on my pinephone to
run it. Note I use [sxmo](https://sxmo.org) and so you
will have to find an alternative to `sxmo_rtcwake.sh` if
you do not.

In the crontab I have:

```cron
*/5 * * * * /usr/bin/env RUST_LOG=trace XDG_RUNTIME_DIR=/run/user/1000/ sxmo_rtcwake.sh /home/alarm/.local/bin/locator_run.sh
```

Which:

 * Configures the environment to increase logging and to
   know where the runtime dir is.
 * Calls `sxmo_rtcwake.sh` which wakes the system up from suspend
   and automatically puts it to sleep
 * Calls a custom scripts

`locator_run.sh` looks as as follows:

```sh
#!/usr/bin/env bash

# Set a logging dir.
#
# Change this to wherever you would like
# your logs.
LOG=${HOME}/.cache/sxmo/locator.log

# Clears the log
echo "" > ${LOG}

# Sometimes on wake the modem fails to connect.
# Here we restart the modem
fixmodem() {
    # Restarts the modem.
	sxmo_modemmonitortoggle.sh restart &> $LOG

	# Unlocks the modem. This is only necassary if
	# you have a pin on your modem.
	nmcli -i 0 --pin ${PIN} &> $LOG
}

# Checks if we can get network connection
#
# Replace 1.1.1.1 with any server you trust to be up.
#
# If we cannot get network we run the fixmodem function to fix it.
ping -c 1 1.1.1.1 ||
	fixmodem ||
	echo "Failed to get WiFi" >> $LOG

locator_cli conn send -a &> $LOG
```
 
# AUTHORS

Written by John Toohey <john_t@mailo.com>.

**Source Code**: <https://gitlab.com/locator1/client>

# BUGS

Submit bug reports online at gitlab:

<https://gitlab.com/locator1/client/-/issues>

# CONFIGURATION

The configuration file lives in `~/.config/locator.yaml`.

For all normal usecase you should never access the file.

Warning, your password is stored in plaintext.

# SEE ALSO

 * jq(1)
