use std::sync::Arc;

use ::api::apis::configuration::Configuration;
use ::api::apis::default_api as api;
use ::api::apis::Error as ApiError;
use ::api::models;
use thiserror::Error;

use crate::config::{Config, ConfigError};
use crate::Mutex;

pub type ClientOuter = Arc<Mutex<Option<Client>>>;

pub struct Client {
    pub key: Option<String>,
    pub client: Configuration,
    pub nominatim: nominatim::Client,
    pub config: Config,
}

impl Client {
    #[must_use]
    pub fn new(url: String) -> Self {
        Self {
            client: {
                Configuration {
                    base_path: url.clone(),
                    ..Configuration::new()
                }
            },
            nominatim: nominatim::Client::new(
                reqwest::Url::parse("https://nominatim.openstreetmap.org/")
                    .unwrap(),
                crate::ID.to_string(),
                Some("john_t@mailo.com".to_string()),
            )
            .unwrap(),
            key: None,
            config: Config::with_server(url),
        }
    }

    /// Creates a client with a config predefined.
    ///
    /// # Errors
    ///
    /// Returns an error if the config does not have a url.
    pub fn with_config(config: Config) -> Result<Self, ConfigError> {
        Ok(Self {
            client: {
                Configuration {
                    base_path: config
                        .server
                        .as_ref()
                        .ok_or(ConfigError::NoServer)?
                        .to_string(),
                    ..Configuration::new()
                }
            },
            nominatim: nominatim::Client::new(
                reqwest::Url::parse(config.nominatim()).unwrap(),
                crate::ID.to_string(),
                Some("john_t@mailo.com".to_string()),
            )
            .unwrap(),
            config,
            key: None,
        })
    }

    /// Logs into a client
    ///
    /// # Errors
    ///
    /// Returns an error if the login fails with the server.
    pub async fn login(&mut self) -> Result<(), LoginError> {
        use models::Login;

        let login = Login {
            username: self
                .config
                .username()
                .map_err(LoginError::ConfigError)?
                .to_string(),
            password: self
                .config
                .password()
                .map_err(LoginError::ConfigError)?
                .to_string(),
        };

        let response = api::login_login_post(&self.client, login).await;
        match response {
            Ok(s) => {
                #[cfg(feature = "web-sys")]
                {
                    let session_storage = web_sys::window()
                        .ok_or(LoginError::SessionStorageError)?
                        .session_storage()
                        .map_err(|_| LoginError::SessionStorageError)?
                        .ok_or(LoginError::SessionStorageError)?;
                    session_storage
                        .set("login", &s)
                        .map_err(|_| LoginError::SessionStorageError)?;
                }
                self.key = Some(s);
                Ok(())
            }
            Err(ApiError::ResponseError(error)) if error.status == 403 => {
                Err(LoginError::InvalidUsernameAndOrPassword)
            }
            Err(e) => Err(LoginError::Api(e)),
        }
    }

    /// Logs into the server if not logged in already
    ///
    /// # Errors
    ///
    /// Returns an error if the login fails.
    pub async fn assert_login(&mut self) -> Result<(), LoginError> {
        if self.key.is_none() {
            #[cfg(feature = "web-sys")]
            {
                let session_storage = web_sys::window()
                    .ok_or(LoginError::SessionStorageError)?
                    .session_storage()
                    .map_err(|_| LoginError::SessionStorageError)?
                    .ok_or(LoginError::SessionStorageError)?;
                let string = session_storage
                    .get("login")
                    .map_err(|_| LoginError::SessionStorageError)?;
                if let Some(string) = string {
                    self.key = Some(string);
                    return Ok(());
                }
            }
            self.login().await
        } else {
            Ok(())
        }
    }
}

#[derive(Debug, Error)]
pub enum LoginError {
    #[error("invalid username and or password")]
    InvalidUsernameAndOrPassword,
    #[error("configuration error: {0}")]
    ConfigError(ConfigError),
    #[error("api error: {0}")]
    Api(ApiError<api::LoginLoginPostError>),
    #[error("session storage error")]
    SessionStorageError,
}
