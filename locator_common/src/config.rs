use std::path::PathBuf;
use std::time::Duration;

use directories::BaseDirs;
use rsa::{RsaPrivateKey, RsaPublicKey};
use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Deserialize, Serialize, Default, Clone, Debug)]
pub struct Config {
    pub nominatim: Option<String>,
    pub server: Option<String>,
    pub username: Option<String>,
    pub password: Option<String>,
    pub private_key: Option<RsaPrivateKey>,
    pub incoming_duration: Option<Duration>,
    pub outgoing_duration: Option<Duration>,
}

impl Config {
    pub fn with_server(server: String) -> Self {
        Self {
            server: Some(server),
            ..Self::default()
        }
    }

    /// Gets the server from this.
    ///
    /// # Errors
    ///
    /// Returns an error if there is no server.
    pub fn server(&self) -> Result<&str, ConfigError> {
        self.server.as_deref().ok_or(ConfigError::NoServer)
    }

    /// Gets the username from this.
    ///
    /// # Errors
    ///
    /// Returns an error if there is no username.
    pub fn username(&self) -> Result<&str, ConfigError> {
        self.username.as_deref().ok_or(ConfigError::NoUsername)
    }

    /// Gets the password from this.
    ///
    /// # Errors
    ///
    /// Returns an error if there is no password.
    pub fn password(&self) -> Result<&str, ConfigError> {
        self.password.as_deref().ok_or(ConfigError::NoPassword)
    }

    /// Gets the `private_key` from this.
    ///
    /// # Errors
    ///
    /// Returns an error if there is no private key.
    pub fn private_key(&self) -> Result<&RsaPrivateKey, ConfigError> {
        self.private_key.as_ref().ok_or(ConfigError::NoPrivateKey)
    }

    /// Gets the `public_key` from this.
    ///
    /// # Errors
    ///
    /// Returns an error if there is no public key.
    pub fn public_key(&self) -> Result<&RsaPublicKey, ConfigError> {
        self.private_key.as_deref().ok_or(ConfigError::NoPublicKey)
    }

    /// Gets the `public_key` from this. This will not return `NoPrivateKey`
    /// but rather return `NoPublicKey` as it will generate a public key
    /// from the private one.
    ///
    /// # Errors
    ///
    /// Returns an error if there is no public key or private key.
    pub fn public_key_or_gen(&mut self) -> Result<&RsaPublicKey, ConfigError> {
        self.private_key.as_deref().ok_or(ConfigError::NoPublicKey)
    }

    /// Gets the nominatim server. If it doesn't exist it will use
    /// `https://nominatim.openstreetmap.org/`
    #[must_use]
    pub fn nominatim(&self) -> &str {
        self.nominatim
            .as_deref()
            .unwrap_or("https://nominatim.openstreetmap.org/")
    }

    /// Gets the `incoming_duration`. If no value is found then 60 will
    /// be used.
    #[must_use]
    pub fn incoming_duration(&self) -> Duration {
        self.incoming_duration
            .unwrap_or_else(|| Duration::from_secs(60))
    }
}

/// An enum to represent a problem with a config
#[derive(Error, Debug, Clone, Eq, PartialEq)]
#[cfg_attr(feature = "serde_errors", derive(Serialize, Deserialize))]
pub enum ConfigError {
    #[error("no server in configuration.")]
    NoServer,
    #[error("no username in configuration.")]
    NoUsername,
    #[error("no password in configuration.")]
    NoPassword,
    #[error("no private key in configuration.")]
    NoPrivateKey,
    #[error("no public key in configuration.")]
    NoPublicKey,
}

#[must_use]
pub fn get_config() -> PathBuf {
    // Create the path for the configuration
    let mut path = PathBuf::new();
    if let Some(base_dirs) = BaseDirs::new() {
        path.push(base_dirs.config_dir());
    }
    path.push("locator.yaml");
    path
}
