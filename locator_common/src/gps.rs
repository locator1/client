use ::rsa::PaddingScheme;
use api::models::Gps;
use chrono::DateTime;
use chrono::FixedOffset;
use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DecryptedGps {
    pub longitude: Option<f64>,
    pub latitude: Option<f64>,
    pub accuracy: Option<f64>,
    pub date: Option<chrono::DateTime<FixedOffset>>,
}

/// A trait holding functions common to all Gps methos.
pub trait GpsLike {
    /// Gets the longitude of the coordinates
    fn longitude(&self) -> Option<f64>;
    /// Gets the latitude of the coordinates
    fn latitude(&self) -> Option<f64>;
    /// Gets the accuracy (in meters) of the coordinates.
    fn accuracy(&self) -> Option<f64>;
    /// Creates a GPS from this.
    fn create_gps(&self) -> DecryptedGps;
}

impl GpsLike for DecryptedGps {
    fn longitude(&self) -> Option<f64> {
        self.longitude
    }

    fn latitude(&self) -> Option<f64> {
        self.latitude
    }

    fn accuracy(&self) -> Option<f64> {
        self.accuracy
    }

    fn create_gps(&self) -> DecryptedGps {
        self.clone()
    }
}

#[derive(Error, Debug)]
pub enum DecryptError {
    #[error("longitude invalid format")]
    LongitudeInvalidFormat,
    #[error("latitude invalid format")]
    LatitudeInvalidFormat,
    #[error("rsa error: {0}")]
    RsaError(#[from] ::rsa::errors::Error),
    #[error("Chrono parse error: {0}")]
    ChronoParseErr(#[from] chrono::format::ParseError),
}

impl DecryptedGps {
    pub fn from_gps(
        gps: Gps,
        key: &rsa::RsaPrivateKey,
    ) -> Result<Self, DecryptError> {
        Ok(Self {
            // Decrypts the longitude
            longitude: gps
                .longitude
                .map(move |data| -> Result<f64, DecryptError> {
                    let padding = PaddingScheme::new_pkcs1v15_encrypt();
                    Ok(f64::from_be_bytes(
                        key.decrypt(padding, &data)?.try_into().map_err(
                            |_| DecryptError::LongitudeInvalidFormat,
                        )?,
                    ))
                })
                .transpose()?,

            // Decrypts the latitude
            latitude: gps
                .latitude
                .map(move |data| -> Result<f64, DecryptError> {
                    let padding = PaddingScheme::new_pkcs1v15_encrypt();
                    Ok(f64::from_be_bytes(
                        key.decrypt(padding, &data)?
                            .try_into()
                            .map_err(|_| DecryptError::LatitudeInvalidFormat)?,
                    ))
                })
                .transpose()?,

            // Parses the data
            date: gps
                .date
                .as_deref()
                .map(DateTime::parse_from_rfc3339)
                .transpose()?,
            accuracy: None,
        })
    }
}
