cfg_if::cfg_if! {
    if #[cfg(any(feature = "tokio", feature = "async-std"))] {
        pub mod client;
        pub mod tasks;
        #[cfg(feature = "streams")]
        pub mod streams;
        pub mod config;
        pub mod rsa_key_cache;
        pub mod gps;
        pub mod error;
        pub use config::Config;
        pub use client::{Client, ClientOuter};
        pub use config::get_config;
        pub use gps::DecryptedGps;
    }
}

#[cfg(all(not(feature = "async-std"), not(feature = "tokio")))]
compile_error!(
    "Not async runtime specified! Specify tokio if on desktop or
    async-std if on wasm."
);

#[cfg(all(not(feature = "web-sys"), target_arch = "wasm32"))]
compile_error!("Please select the web-sys feature if on wasm");

pub static ID: &str = "org.john_t.Locator";

#[cfg(feature = "async-std")]
pub(crate) use async_std::sync::Mutex;
#[cfg(feature = "tokio")]
pub(crate) use tokio::sync::Mutex;
