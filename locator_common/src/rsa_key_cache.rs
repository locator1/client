use std::collections::HashMap;
use std::sync::Arc;

use ::api::apis::default_api as api;
use ::api::apis::Error as ApiError;
use rsa::{
    pkcs8::{self, FromPublicKey},
    RsaPublicKey,
};
use thiserror::Error;

use crate::client::Client;
use crate::Mutex;

pub type RsaKeyCacheOuter = Arc<Mutex<RsaKeyCache>>;

#[derive(Default, Clone)]
pub struct RsaKeyCache {
    cache: HashMap<String, RsaPublicKey>,
}

impl RsaKeyCache {
    /// Creates a new `RsaKeyCache`
    #[must_use]
    pub fn new() -> Self {
        Self::default()
    }

    /// Gets the key for a user
    pub async fn key(
        &mut self,
        user: &str,
        client: &Client,
    ) -> Result<&RsaPublicKey, GetRsaKeyError> {
        // Check if we already have this key
        if !self.cache.contains_key(user) {
            // If not then we will get it from the server
            let key = api::login_users_user_rsa_key_get(&client.client, user)
                .await
                .map_err(GetRsaKeyError::Api)?;

            let key = RsaPublicKey::from_public_key_der(&key)
                .map_err(GetRsaKeyError::RsaKey)?;
            self.cache.insert(user.to_owned(), key);

            return Ok(self.cache.get(user).unwrap());
        }

        // Or just return the cache
        Ok(self.cache.get(user).unwrap())
    }
}

#[derive(Error, Debug)]
pub enum GetRsaKeyError {
    #[error("api error: {0}")]
    Api(ApiError<api::LoginUsersUserRsaKeyGetError>),
    #[error("rsa key error: {0}")]
    RsaKey(pkcs8::Error),
}
