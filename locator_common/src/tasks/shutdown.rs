use crate::error::Error;
#[cfg(feature = "streams")]
use crate::streams::Stream;
#[cfg(feature = "daemon")]
use crate::tasks::Task;
#[cfg(feature = "daemon")]
use std::path::PathBuf;
#[cfg(feature = "daemon")]
use std::env;

/// Turns of any existing daemons.
///
/// # Errors
///
/// Fails if:
///  - The variable `XDG_RUNTIME_DIR` is not set
///  - Sending information to the stream fails.
pub async fn shutdown() -> Result<(), Error> {
    #[cfg(feature = "streams")]
    {
        let mut path: PathBuf = env::var("XDG_RUNTIME_DIR")?.parse().unwrap();
        path.push("locator.sock");
        log::info!("Connecting...");

        // If we can't get a stream thats fine, because it probably
        // just means there aren't any.
        if let Ok(stream) = Stream::connect(path).await {
            log::info!("Sending...");
            stream.send(&Task::Shutdown).await?;
        }
    }
    log::info!("Done!");

    Ok(())
}
