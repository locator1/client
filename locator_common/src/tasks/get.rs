use ::api::apis::default_api as api;
use ::api::models;

use crate::error::Error;
use crate::tasks::TaskMode;
#[cfg(feature = "daemon")]
use crate::tasks::Task;

/// Gets the connections from the server.
pub async fn get_connections(
    task_mode: &TaskMode,
) -> Result<Vec<models::ListConnectionsItem>, Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let client = client.lock().await;
            let client = client.as_ref().ok_or_else(Error::no_client)?;

            let key = client.key.as_deref().ok_or_else(Error::no_key)?;
            api::gps_get(&client.client, key).await.map_err(Into::into)
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            let result: Result<Vec<models::ListConnectionsItem>, Error> =
                stream.send_and_receive(&Task::Shutdown).await?;
            result.map_err(|x| x)
        }
    }
}
