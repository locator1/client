use std::sync::Arc;

#[cfg(feature = "async-std")]
use async_std::sync::Mutex;
#[cfg(feature = "tokio")]
use tokio::sync::Mutex;

use crate::rsa_key_cache::RsaKeyCacheOuter;
#[cfg(feature = "streams")]
use crate::streams::Stream;
use crate::tasks::acquire_client;
use crate::ClientOuter;
#[cfg(feature = "daemon")]
use std::path::PathBuf;
#[cfg(feature = "daemon")]
use std::env;

/// Determines how to execute tasks.
///
/// `Local` is always available, but `Daemon` requires the `daemon`
/// feature.
#[derive(Clone)]
pub enum TaskMode {
    /// Tasks will be sent to a daemon process.
    #[cfg(feature = "daemon")]
    Daemon {
        stream: crate::streams::Stream<PathBuf>,
    },
    /// Tasks will be executed locally.
    Local {
        client: ClientOuter,
        rsa_key_cache: RsaKeyCacheOuter,
    },
}

impl TaskMode {
    /// Creates a new [`TaskMode`] using a daemon if available,  otherwise
    /// it will make a [`ClientOuter`] and [`RsaKeyCacheOuter`]
    pub async fn new() -> Self {
        #[cfg(feature = "daemon")]
        {
            if let Ok(path) = env::var("XDG_RUNTIME_DIR") {
                let mut path: PathBuf = path.parse().unwrap();
                path.push("locator.sock");
                log::info!("Connecting...");

                // If we can't get a stream thats fine, because it probably
                // just means there isn't any.
                if let Ok(stream) = Stream::connect(path).await {
                    Self::Daemon { stream }
                } else {
                    log::info!("Using local task mode.");
                    Self::local().await
                }
            } else {
                log::info!("Using local task mode.");
                Self::local().await
            }
        }
        #[cfg(not(feature = "daemon"))]
        {
            Self::local().await
        }
    }

    /// Creates a new, local [`TaskMode`]
    pub async fn local() -> Self {
        let me = Self::Local {
            client: Arc::new(Mutex::new(None)),
            rsa_key_cache: RsaKeyCacheOuter::default(),
        };
        acquire_client::acquire_client(&me).await.ok();
        me
    }

    /// Creates a new, local [`TaskMode`] which is
    /// not logged in.
    pub fn local_not_logged_in() -> Self {
        Self::Local {
            client: Arc::new(Mutex::new(None)),
            rsa_key_cache: RsaKeyCacheOuter::default(),
        }
    }
}
