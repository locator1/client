use ::api::apis::default_api as api;
use ::api::models;
use rsa::pkcs8::ToPublicKey;
pub use thiserror::Error;

use crate::error::Error;
use crate::tasks::TaskMode;
#[cfg(feature = "daemon")]
use crate::tasks::Task;

/// Signs a user up to locator.
///
/// This will set the `key` on the client.
///
/// # Errors
///
///  * An incomplete configuration.
///  * Incorrect credentials.
///  * Incorrect api.
pub async fn login(task: &TaskMode, new_key: bool) -> Result<(), Error> {
    log::info!("Logging in!");
    match task {
        TaskMode::Local { client, .. } => {
            let mut client = client.lock().await;
            let mut client = client.as_mut().ok_or_else(Error::no_client)?;

            log::debug!("Creating a `Login`...");
            let login = models::Login {
                username: client.config.username()?.to_owned(),
                password: client.config.password()?.to_owned(),
            };

            // Attempt to log in.
            log::debug!("Sending the login request...");
            let session = api::login_login_post(&client.client, login).await?;
            log::debug!("Received the login request.");
            client.key = Some(session);

            if new_key {
                // Submit out RSA key.
                log::debug!("Sending RSA key...");
                api::login_users_user_rsa_key_put(
                    &client.client,
                    client.config.username()?,
                    client.key.as_ref().ok_or_else(Error::no_key)?,
                    client
                        .config
                        .private_key()?
                        .to_public_key_der()
                        .unwrap()
                        .as_ref()
                        .to_vec(),
                )
                .await?;
            }

            log::debug!("Done.");
            Ok(())
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream.send_and_receive(&Task::Login(new_key)).await?
        }
    }
}
