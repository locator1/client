use crate::error::Error;
use crate::tasks::TaskMode;
#[cfg(feature = "daemon")]
use crate::tasks::Task;

/// Clears the client from the `TaskMode`
///
/// This is useful when you want to create a new configuration. Note
/// it will only every return an error when running in daemon mode,
/// otherwise it will always work.
pub async fn clear_client(task_mode: &TaskMode) -> Result<(), Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let mut client = client.lock().await;
            *client = None;
            log::info!("Cleared client...");
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream.send(&Task::ClearClient).await?;
        }
    }

    Ok(())
}
