use std::borrow::Cow;

use ::api::apis::default_api as api;
use ::api::models;
use chrono::Utc;
pub use models::ListConnectionsItem;
use rand::rngs::OsRng;
use rsa::{PaddingScheme, PublicKey};

use crate::error::Error;
use crate::gps::{DecryptedGps, GpsLike};
use crate::tasks::TaskMode;
#[cfg(feature = "daemon")]
use crate::tasks::Task;

/// Gets the GPS information for a connection.
pub async fn get(
    task_mode: &TaskMode,
    sender: impl Into<Cow<'_, str>>,
) -> Result<DecryptedGps, Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let client = client.lock().await;
            let client = client.as_ref().ok_or_else(Error::no_client)?;

            let gps = api::gps_by_sender_sender_get(
                &client.client,
                &sender.into(),
                client.key.as_ref().ok_or_else(Error::no_key)?,
            )
            .await?;

            DecryptedGps::from_gps(gps, client.config.private_key()?)
                .map_err(Into::into)
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream
                .send_and_receive(&Task::GpsGet(sender.into().into_owned()))
                .await?
        }
    }
}

/// Sends the GPS information to a connection.
pub async fn send(
    task_mode: &TaskMode,
    gps: impl GpsLike,
    user: impl Into<Cow<'_, str>>,
) -> Result<(), Error> {
    match task_mode {
        TaskMode::Local {
            client,
            rsa_key_cache,
        } => {
            let user = user.into();

            // Get out concurrent structures.
            let (mut rsa_key_cache, client) =
                futures::join!(rsa_key_cache.lock(), client.lock(),);

            let mut rng = OsRng;
            let client = client.as_ref().ok_or_else(Error::no_client)?;
            let key = rsa_key_cache.key(&user, client).await?;

            let mut latitude = None;
            let mut longitude = None;

            if let Some(lat) = gps.latitude() {
                // Convert to bytes to allow encryption
                let lat = lat.to_be_bytes();

                // Get the padding schemes
                let lat_padding = PaddingScheme::new_pkcs1v15_encrypt();

                // Encrypt it
                latitude =
                    Some(key.encrypt(&mut rng, lat_padding, &lat[..])?);
            }

            if let Some(long) = gps.longitude() {
                // Convert to bytes to allow encryption
                let long = long.to_be_bytes();

                // Get the padding schemes
                let long_padding = PaddingScheme::new_pkcs1v15_encrypt();

                // Encrypt it
                longitude =
                    Some(key.encrypt(&mut rng, long_padding, &long[..])?);
            }

            // Send it to the server
            log::debug!("Sending data...");
            api::gps_by_reciever_reciever_put(
                &client.client,
                &user,
                client.key.as_ref().unwrap(),
                Some(models::Gps {
                    longitude,
                    latitude,
                    date: Some(Utc::now().to_rfc3339()),
                }),
            )
            .await?;

            Ok(())
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream
                .send_and_receive(&Task::SendLocation {
                    gps: gps.create_gps(),
                    user: user.into().into_owned(),
                })
                .await?
        }
    }
}
