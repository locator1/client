use crate::Client;
use crate::Config;

cfg_if! {
    if #[cfg(feature = "tokio")] {
        use tokio::fs;
    } else if #[cfg(all(feature = "async-std", not(target_arch = "wasm32")))] {
        use async_std::fs;
    }
}

#[cfg(feature = "daemon")]
use crate::tasks::Task;
use cfg_if::cfg_if;

use crate::error::Error;
use crate::tasks::{self, TaskMode};

/// Gets the config and sets it up into a client.
pub async fn acquire_client(task_mode: &TaskMode) -> Result<(), Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            cfg_if! {
                if #[cfg(feature = "web")] {

                    // Get the config string.
                    let window = web_sys::window()
                        .ok_or_else(Error::local_storage)?;

                    let local_storage = window
                        .local_storage()
                        .map_err(|_| Error::local_storage())?
                        .ok_or_else(Error::local_storage)?;

                    let config = local_storage
                        .get_item(crate::ID)
                        .map_err(|_| Error::local_storage())?
                        .ok_or_else(Error::local_storage)?;

                    let config: Config = serde_yaml::from_str(&config)?;
                } else {
                    log::info!("Found config file: {:?}", crate::get_config());

                    // Try and parse the config
                    let config: Config = serde_yaml::from_str(
                        &fs::read_to_string(crate::get_config()).await?,
                    )?;
                }
            }

            // Create a client
            log::debug!("Creating new client...");
            let client_new = Client::with_config(config)?;
            log::debug!("Created new client.");

            log::debug!("Getting borrow...");
            let mut client_borrow = client.lock().await;
            log::debug!("Got borrow.");

            log::debug!("Setting borrow...");
            *client_borrow = Some(client_new);
            log::debug!("Set borrow.");

            // Must be dropped here or we create a lock as
            // login will never finish.
            drop(client_borrow);

            log::debug!("Logging in...");
            tasks::login(task_mode, false).await?;
            log::debug!("Done.");

            Ok(())
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream.send_and_receive(&Task::AcquireClient).await?
        }
    }
}

/// Creates a client from a configuration.
pub async fn client_with_config(
    task_mode: &TaskMode,
    config: Config,
) -> Result<(), Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            log::info!("Creating client with config...");
            let mut client = client.lock().await;
            *client = Some(Client::with_config(config)?);
            Ok(())
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream
                .send_and_receive(&Task::ClientWithConfig(config))
                .await?
        }
    }
}
