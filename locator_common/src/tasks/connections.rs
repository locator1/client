use std::borrow::Cow;

use ::api::apis::default_api as api;
use ::api::models;
pub use models::{Gps, ListConnectionsItem};

use crate::config::ConfigError;
use crate::error::Error;
use crate::tasks::{self, TaskMode};
#[cfg(feature = "daemon")]
use crate::tasks::Task;

/// Adds a new connections.
pub async fn add_connection(
    task_mode: &TaskMode,
    connection: impl Into<Cow<'_, str>>,
) -> Result<(), Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let client = client.lock().await;
            let client = client.as_ref().ok_or_else(Error::no_client)?;

            api::gps_post(
                &client.client,
                client.key.as_ref().ok_or_else(Error::no_key)?,
                &connection.into(),
            )
            .await?;

            Ok(())
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream
                .send_and_receive(&Task::AddConnection(
                    connection.into().into_owned(),
                ))
                .await?
        }
    }
}

/// Remove an existing connections.
pub async fn remove_connection_from_sender(
    task_mode: &TaskMode,
    connection: impl Into<Cow<'_, str>>,
) -> Result<(), Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let client = client.lock().await;
            let client = client.as_ref().ok_or_else(Error::no_client)?;

            let result = api::gps_by_sender_sender_delete(
                &client.client,
                &connection.into(),
                client.key.as_ref().ok_or_else(Error::no_key)?,
            )
            .await;

            result?;

            Ok(())
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream
                .send_and_receive(&Task::RemoveConnectionFromSender(
                    connection.into().into_owned(),
                ))
                .await?
        }
    }
}

/// Remove an existing connections.
pub async fn remove_connection_from_reciever(
    task_mode: &TaskMode,
    connection: impl Into<Cow<'_, str>>,
) -> Result<(), Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let client = client.lock().await;
            let client = client.as_ref().ok_or_else(Error::no_client)?;

            let result = api::gps_by_reciever_reciever_delete(
                &client.client,
                &connection.into(),
                client.key.as_ref().ok_or_else(Error::no_key)?,
            )
            .await;

            result?;

            Ok(())
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream
                .send_and_receive(&Task::RemoveConnectionFromReceiver(
                    connection.into().into_owned(),
                ))
                .await?
        }
    }
}

/// Gains a list of all the connections.
pub async fn list_connections(
    task_mode: &TaskMode,
) -> Result<Vec<ListConnectionsItem>, Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let client = client.lock().await;
            let client = client.as_ref().ok_or_else(Error::no_client)?;

            api::gps_get(
                &client.client,
                client.key.as_ref().ok_or_else(Error::no_key)?,
            )
            .await
            .map_err(Into::into)
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream.send_and_receive(&Task::ListConnections).await?
        }
    }
}

/// Removes a [`ListConnectionsItem`] connection.
///
/// This will remove assuming we are username. You should use this
/// if you are making multiple requests, otherwise just use
/// [`remove_list_item`]
pub async fn remove_list_item_with_username(
    tm: &TaskMode,
    lci: ListConnectionsItem,
    username: &str,
) -> Result<(), Error> {
    if lci.sender == username {
        remove_connection_from_reciever(tm, lci.reciever).await?;
    } else {
        remove_connection_from_sender(tm, lci.sender).await?;
    }

    Ok(())
}

/// Removes all current connections
pub async fn remove_current_connections(tm: &TaskMode) -> Result<(), Error> {
    let username = tasks::config::get_username(tm)
        .await?
        .ok_or(ConfigError::NoUsername)?;
    let conns = list_connections(tm).await?;

    // Loop through every connection and send them separately.
    //
    // This will exit at the first failed attempt.
    futures::future::try_join_all(conns.into_iter().map(|conn| {
        // Fix the borrow checker
        let username = &username;
        let tm = &tm;

        // Return the function which must be called
        remove_list_item_with_username(tm, conn, username)
    }))
    .await?;

    Ok(())
}
