use ::api::apis::default_api as api;
use ::api::models;
use rsa::pkcs8::ToPublicKey;
pub use thiserror::Error;

use crate::error::Error;
use crate::tasks::TaskMode;
#[cfg(feature = "daemon")]
use crate::tasks::Task;

/// Signs a user up to locator.
///
/// This will set the `key` on the client.
///
/// # Errors
///
///  * An incomplete configuration.
///  * Incorrect api.
///  * Clashing usernames.
pub async fn signup(task_mode: &TaskMode) -> Result<(), Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let mut client = client.lock().await;
            let client = client.as_mut().ok_or_else(Error::no_client)?;
            log::debug!("Creating a `LoginUser`...");
            let login_user = models::LoginUser {
                username: client.config.username()?.to_owned(),
                password: client.config.password()?.to_owned(),
                rsa_key: client
                    .config
                    .private_key()?
                    .to_public_key_der()
                    .unwrap()
                    .as_ref()
                    .to_vec(),
            };

            log::debug!("Creating a `Login`...");
            let login = models::Login {
                username: client.config.username()?.to_owned(),
                password: client.config.password()?.to_owned(),
            };

            // Attempt to sign up.
            log::debug!("Sending the signup request...");
            api::login_users_post(&client.client, login_user).await?;
            log::debug!("Sending the login request...");
            let session = api::login_login_post(&client.client, login).await?;
            log::debug!("Signup complete!");
            client.key = Some(session);

            Ok(())
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream.send_and_receive(&Task::Signup).await?
        }
    }
}
