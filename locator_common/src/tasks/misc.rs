use crate::error::Error;
use crate::tasks::TaskMode;
#[cfg(feature = "daemon")]
use crate::tasks::Task;

/// Gets a clone of the key from the client
pub async fn get_key(task_mode: &TaskMode) -> Result<Option<String>, Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let client = client.lock().await;
            let client = client.as_ref().ok_or_else(Error::no_client)?;
            Ok(client.key.clone())
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream.send_and_receive(&Task::GetKey).await?
        }
    }
}

/// Works out if we are logged in or not
///
/// This is infalliable unless the `daemon` feature is enabled.
pub async fn has_key(tm: &TaskMode) -> Result<bool, Error> {
    match tm {
        TaskMode::Local { client, .. } => {
            let client = client.lock().await;
            let client = client.as_ref();
            if let Some(client) = client {
                Ok(client.key.is_some())
            } else {
                Ok(false)
            }
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream.send_and_receive(&Task::HasKey).await?
        }
    }
}
