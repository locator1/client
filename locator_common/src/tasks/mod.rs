use serde::{Deserialize, Serialize};
use thiserror::Error;

pub mod signup;
pub use signup::signup;

pub mod login;
pub use login::login;

pub mod acquire_client;
pub use acquire_client::{acquire_client, client_with_config};

pub mod get;

pub mod shutdown;
pub use shutdown::shutdown;

pub mod config;
pub use config::mod_config;

pub mod task_mode;
pub use task_mode::TaskMode;

pub mod clear_client;
pub use clear_client::clear_client;

pub mod write_config;
pub use write_config::write_config;

pub mod connections;
pub use connections::{
    add_connection, list_connections, remove_connection_from_reciever,
    remove_connection_from_sender,
};

pub mod gps;
use crate::DecryptedGps;

pub mod nominatim;

pub mod misc;
pub use misc::get_key;

#[derive(Debug, Error, Deserialize)]
#[error("no client")]
pub struct NoClientError;

#[derive(Serialize, Deserialize, Debug)]
pub enum Task {
    /// Closes the app.
    Shutdown,
    /// Sets the username
    ModConfig {
        mask: config::ConfigMask,
        config: crate::Config,
    },
    /// Signs up with a configuration
    Signup,
    /// Logs in with the configuration
    ///
    /// The bool is whether it should generate a new RSA key (so false)
    Login(bool),
    /// Clears any clients
    ClearClient,
    /// Creates a client with a configuration.
    ClientWithConfig(crate::Config),
    /// Gets the config and sets it up into a client.
    AcquireClient,
    /// Writes the configuration file.
    WriteConfig,
    /// Adds a connection
    AddConnection(String),
    /// Removes a connection by sender
    RemoveConnectionFromSender(String),
    /// Removes a connection by receiver
    RemoveConnectionFromReceiver(String),
    /// Lists all of the connections
    ListConnections,
    /// Gets a gps connection
    GpsGet(String),
    /// Sends the location to a user
    SendLocation { gps: DecryptedGps, user: String },
    /// Gets some fields from a Config
    GetConfig(config::ConfigMask),
    /// Does literally nothing
    Noop,
    /// Reverse geocodes an address
    NominatimReverseGeocode { longitude: f64, latitude: f64 },
    /// Gets a clone of the key from the client
    GetKey,
    /// Works out if we are logged in or not.
    HasKey,
    /// Logs the user out, deleting their config
    Logout,
}
