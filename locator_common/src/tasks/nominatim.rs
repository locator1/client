use crate::error::Error;
use crate::tasks::TaskMode;
#[cfg(feature = "daemon")]
use crate::tasks::Task;

/// Reverse geocodes an address.
pub async fn reverse_geocode(
    task_mode: &TaskMode,
    longitude: f64,
    latitude: f64,
) -> Result<nominatim::Response, Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let client = client.lock().await;
            let client = client.as_ref().ok_or_else(Error::no_client)?;
            let query = nominatim::ReverseQueryBuilder::default()
                .lon(longitude)
                .lat(latitude)
                .address_details(false)
                .extra_tags(false)
                .zoom(nominatim::Zoom::Building)
                .build()
                .expect(
                    "Failed to build nominatim query. \
                    Please submit a bug report at \
                    https://gitlab.com/locator1/client/-/issues",
                );

            let response = client.nominatim.reverse(query).await?;

            Ok(response)
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream
                .send_and_receive(&Task::NominatimReverseGeocode {
                    longitude,
                    latitude,
                })
                .await?
        }
    }
}
