use bitflags::bitflags;
use serde::{Deserialize, Serialize};

use crate::error::Error;
use crate::tasks::TaskMode;
use crate::Config;
#[cfg(feature = "daemon")]
use crate::tasks::Task;

cfg_if::cfg_if! {
    if #[cfg(feature = "tokio")] {
        use tokio::fs;
    } else if #[cfg(all(feature = "async-std", not(target_arch = "wasm32")))] {
        use async_std::fs;
    }
}

bitflags! {
    #[derive(Serialize, Deserialize)]
    pub struct ConfigMask: u16 {
        const NOMINATIM = 2_u16.pow(0);
        const SERVER = 2_u16.pow(1);
        const USERNAME = 2_u16.pow(2);
        const PASSWORD = 2_u16.pow(3);
        const PRIVATEKEY = 2_u16.pow(4);
    }
}

/// Modifies the config, applying the fields of one to the other.
pub async fn mod_config(
    task_mode: &TaskMode,
    mask: ConfigMask,
    new: Config,
) -> Result<(), Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let mut client = client.lock().await;
            let client = client.as_mut().ok_or_else(Error::no_client)?;
            mod_config_directly(&mut client.config, mask, new).await;
            Ok(())
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream
                .send_and_receive(&Task::ModConfig { mask, config: new })
                .await?
        }
    }
}

/// Modifies the config, applying the fields of one to the other.
async fn mod_config_directly(
    config: &mut Config,
    mask: ConfigMask,
    new: Config,
) {
    if mask.contains(ConfigMask::NOMINATIM) {
        config.nominatim = new.nominatim;
    }

    if mask.contains(ConfigMask::SERVER) {
        config.server = new.server;
    }

    if mask.contains(ConfigMask::USERNAME) {
        config.username = new.username;
    }

    if mask.contains(ConfigMask::PASSWORD) {
        config.password = new.password;
    }

    if mask.contains(ConfigMask::PRIVATEKEY) {
        config.private_key = new.private_key;
    }
}

/// Gets a masked version of a [`Config`]
///
/// To prevent sending lots over a wire of excess overhead you can
/// use a [`ConfigMask`] to specify the fields you want, otherwise
/// they will undoubtedly be `None`.
pub async fn get(
    task_mode: &TaskMode,
    mask: ConfigMask,
) -> Result<Config, Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let mut client = client.lock().await;
            let client = client.as_mut().ok_or_else(Error::no_client)?;
            let mut config = Config::default();

            if mask.contains(ConfigMask::NOMINATIM) {
                config.nominatim = client.config.nominatim.clone();
            }

            if mask.contains(ConfigMask::SERVER) {
                config.server = client.config.server.clone();
            }

            if mask.contains(ConfigMask::USERNAME) {
                config.username = client.config.username.clone();
            }

            if mask.contains(ConfigMask::PASSWORD) {
                config.password = client.config.password.clone();
            }

            if mask.contains(ConfigMask::PRIVATEKEY) {
                config.private_key = client.config.private_key.clone();
            }

            Ok(config)
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream.send_and_receive(&Task::GetConfig(mask)).await?
        }
    }
}

macro_rules! mod_n {
    ($prop:ident, $flag:ident, $name:ident, $typ:ty) => {
        /// Modifies the config property.
        ///
        /// This is a wrapper around [`mod_config`]
        pub async fn $name(
            task_mode: &TaskMode,
            string: Option<$typ>,
        ) -> Result<(), Error> {
            let config = Config {
                $prop: string,
                ..Config::default()
            };

            mod_config(task_mode, ConfigMask::$flag, config).await
        }
    };
}

mod_n!(nominatim, NOMINATIM, mod_nominatim, String);
mod_n!(server, SERVER, mod_server, String);
mod_n!(username, USERNAME, mod_username, String);
mod_n!(password, PASSWORD, mod_password, String);
mod_n!(private_key, PRIVATEKEY, mod_private_key, rsa::RsaPrivateKey);

macro_rules! get_n {
    ($prop:ident, $flag:ident, $name:ident, $typ:ty) => {
        /// Modifies the config property.
        ///
        /// This is a wrapper around [`get`]
        pub async fn $name(
            task_mode: &TaskMode,
        ) -> Result<Option<$typ>, Error> {
            Ok(get(task_mode, ConfigMask::$flag).await?.$prop)
        }
    };
}

get_n!(nominatim, NOMINATIM, get_nominatim, String);
get_n!(server, SERVER, get_server, String);
get_n!(username, USERNAME, get_username, String);
get_n!(password, PASSWORD, get_password, String);
get_n!(private_key, PRIVATEKEY, get_private_key, rsa::RsaPrivateKey);

/// Logs the user out, deleting their configuration file.
pub async fn logout(tm: &TaskMode) -> Result<(), Error> {
    match tm {
        TaskMode::Local {client, ..} => {
            let mut client = client.lock().await;
            *client = None;
            
            cfg_if::cfg_if! {
                if #[cfg(feature = "web")] {

                    // Get the config string.
                    let window = web_sys::window()
                        .ok_or_else(Error::local_storage)?;

                    let local_storage = window
                        .local_storage()
                        .map_err(|_| Error::local_storage())?
                        .ok_or_else(Error::local_storage)?;

                    local_storage
                        .remove_item(crate::ID)
                        .map_err(|_| Error::local_storage())?;

                    Ok(())
                } else {
                    log::info!("Found config file: {:?}", crate::get_config());

                    // Remove the file
                    fs::remove_file(crate::get_config()).await?;

                    Ok(())
                }
            }
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream.send_and_receive(&Task::Logout).await?
        }
    }
}
