use cfg_if::cfg_if;

cfg_if! {
    if #[cfg(feature = "tokio")] {
        use tokio::fs;
    } else if #[cfg(all(feature = "async-std", not(target_arch = "wasm32")))] {

    }
}

use crate::error::Error;
use crate::tasks::TaskMode;
#[cfg(feature = "daemon")]
use crate::tasks::Task;

/// Writes the configuration to a file.
pub async fn write_config(task_mode: &TaskMode) -> Result<(), Error> {
    match task_mode {
        TaskMode::Local { client, .. } => {
            let client = client.lock().await;
            let client = client.as_ref().ok_or_else(Error::no_client)?;
            log::info!("Writing config file...");
            cfg_if! {
                if #[cfg(feature = "web")] {
                    let config = serde_json::to_string(&client.config)?;
                    // Write the config string.
                    let window = web_sys::window()
                        .ok_or_else(Error::local_storage)?;

                    let local_storage = window
                        .local_storage()
                        .map_err(|_| Error::local_storage())?
                        .ok_or_else(Error::local_storage)?;

                    local_storage
                        .set_item(crate::ID, &config)
                        .map_err(|_| Error::local_storage())?;
                } else {
                    let config = serde_yaml::to_string(&client.config)?;
                    let config_path = crate::get_config();
                    fs::write(config_path, config.as_bytes())
                    .await?;
                }
            }
            Ok(())
        }
        #[cfg(feature = "daemon")]
        TaskMode::Daemon { stream } => {
            stream.send_and_receive(&Task::WriteConfig).await?
        }
    }
}
