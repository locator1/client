use std::env;
use std::io;

use ::api::apis::Error as ApiError;
use http::status::StatusCode;
use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Error, Debug)]
#[cfg_attr(feature = "serde_errors", derive(Serialize, Deserialize))]
#[error("{err} (category: {category})")]
pub struct Error {
    err: ErrorEnum,
    category: Category,
}

#[derive(Error, Debug)]
#[error("zero bytes received over stream")]
pub struct ZeroBytesOverStream;

#[derive(Error, Debug)]
#[cfg_attr(feature = "serde_errors", derive(Serialize, Deserialize))]
#[cfg_attr(
    feature = "serde_errors",
    serde(from = "ErrorEnumSerde", into = "ErrorEnumSerde")
)]
pub(crate) enum ErrorEnum {
    #[error("{0}")]
    String(String),
    #[error("no client")]
    NoClient,
    #[error("username not found")]
    UsernameNotFound,
    #[error("no key")]
    NoKey,
    #[error("configuration error: {0}")]
    ConfigError(#[from] crate::config::ConfigError),
    #[error("user with selected username already exists")]
    UsernameClash,
    #[error("incorrect username or password")]
    IncorrectUsernameOrPassword,
    #[error("io error: {0}")]
    Io(io::Error),
    #[error("serde error: {0}")]
    SerdeJson(#[from] serde_json::Error),
    #[error("serde error: {0}")]
    SerdeYaml(#[from] serde_yaml::Error),
    #[error("error making reqwest: {0}")]
    Reqwest(#[from] reqwest::Error),
    #[error("error when getting environment variable: {0}")]
    Var(#[from] env::VarError),
    #[error("{err_type}: unexpected {status} returned with body {content}")]
    ResponseError {
        err_type: String,
        status: StatusCode,
        content: String,
    },
    #[error("decoding rsa error: {0}")]
    DecodeRsa(#[from] rsa::pkcs8::Error),
    #[error("rsa error: {0}")]
    Rsa(#[from] rsa::errors::Error),
    #[error("invalid session")]
    InvalidSession,
    #[error("longitude invalid format")]
    LongitudeInvalidFormat,
    #[error("latitude invalid format")]
    LatitudeInvalidFormat,
    #[error("date time failed to parse: {0}")]
    Chrono(chrono::format::ParseError),
    #[cfg(feature = "zbus")]
    #[error("failed to use dbus: {0}")]
    Zbus(#[from] zbus::Error),
    #[error("error fetching from localstorage")]
    LocalStorage,
    #[error("url error")]
    Url(#[from] url::ParseError),
}

impl Clone for ErrorEnum {
    fn clone(&self) -> Self {
        match self {
            Self::String(v) => Self::String((*v).clone()),
            Self::NoClient => Self::NoClient,
            Self::NoKey => Self::NoKey,
            Self::ConfigError(v) => Self::ConfigError((*v).clone()),
            Self::UsernameClash => Self::UsernameClash,
            Self::UsernameNotFound => Self::UsernameNotFound,
            Self::IncorrectUsernameOrPassword => {
                Self::IncorrectUsernameOrPassword
            }
            Self::Io(v) => Self::String(v.to_string()),
            Self::SerdeJson(v) => Self::String(v.to_string()),
            Self::SerdeYaml(v) => Self::String(v.to_string()),
            Self::Reqwest(v) => Self::String(v.to_string()),
            Self::Var(v) => Self::Var((*v).clone()),
            Self::ResponseError {
                err_type,
                status,
                content,
            } => Self::ResponseError {
                err_type: (*err_type).clone(),
                status: (*status),
                content: (*content).clone(),
            },
            Self::Rsa(v) => Self::String(v.to_string()),
            Self::DecodeRsa(v) => Self::DecodeRsa(*v),
            Self::InvalidSession => Self::InvalidSession,
            Self::LongitudeInvalidFormat => Self::LongitudeInvalidFormat,
            Self::LatitudeInvalidFormat => Self::LatitudeInvalidFormat,
            Self::Chrono(v) => Self::Chrono(*v),
            #[cfg(feature = "zbus")]
            Self::Zbus(v) => Self::String(v.to_string()),
            Self::LocalStorage => Self::LocalStorage,
            Self::Url(v) => Self::Url(*v),
        }
    }
}

#[cfg(feature = "serde_errors")]
#[derive(Error, Debug, Serialize, Deserialize)]
enum ErrorEnumSerde {
    #[error("{0}")]
    String(String),
    #[error("no client")]
    NoClient,
    #[error("no key")]
    NoKey,
    #[error("username not found")]
    UsernameNotFound,
    #[error("configuration error: {0}")]
    ConfigError(#[from] crate::config::ConfigError),
    #[error("user with selected username already exists")]
    UsernameClash,
    #[error("incorrect password")]
    IncorrectUsernameOrPassword,
    #[error("{err_type}: unexpected {status} returned with body {content}")]
    ResponseError {
        err_type: String,
        #[serde(with = "StatusCodeWrapper")]
        status: StatusCode,
        content: String,
    },
    #[error("invalid session")]
    InvalidSession,
    #[error("longitude invalid format")]
    LongitudeInvalidFormat,
    #[error("latitude invalid format")]
    LatitudeInvalidFormat,
    #[error("error getting key from local storage")]
    LocalStorage,
}

#[cfg(feature = "serde_errors")]
impl From<ErrorEnumSerde> for ErrorEnum {
    fn from(them: ErrorEnumSerde) -> Self {
        match them {
            ErrorEnumSerde::String(s) => Self::String(s),
            ErrorEnumSerde::NoClient => Self::NoClient,
            ErrorEnumSerde::NoKey => Self::NoKey,
            ErrorEnumSerde::ConfigError(v) => Self::ConfigError(v),
            ErrorEnumSerde::UsernameClash => Self::UsernameClash,
            ErrorEnumSerde::UsernameNotFound => Self::UsernameNotFound,
            ErrorEnumSerde::IncorrectUsernameOrPassword => {
                Self::IncorrectUsernameOrPassword
            }
            ErrorEnumSerde::ResponseError {
                err_type,
                status,
                content,
            } => Self::ResponseError {
                err_type,
                status,
                content,
            },
            ErrorEnumSerde::InvalidSession => Self::InvalidSession,
            ErrorEnumSerde::LatitudeInvalidFormat => {
                Self::LatitudeInvalidFormat
            }
            ErrorEnumSerde::LongitudeInvalidFormat => {
                Self::LongitudeInvalidFormat
            }
            ErrorEnumSerde::LocalStorage => Self::LocalStorage,
        }
    }
}

#[derive(Copy, Clone, Serialize, Deserialize)]
#[serde(remote = "StatusCode")]
pub struct StatusCodeWrapper(#[serde(getter = "StatusCode::as_u16")] u16);

impl From<StatusCode> for StatusCodeWrapper {
    fn from(them: StatusCode) -> Self {
        Self(them.as_u16())
    }
}

impl From<StatusCodeWrapper> for StatusCode {
    fn from(them: StatusCodeWrapper) -> Self {
        StatusCode::try_from(them.0).unwrap()
    }
}

#[derive(Debug, Clone, Copy, Deserialize, Serialize, Error)]
pub enum Category {
    #[error("no client")]
    NoClient,
    #[error("no key")]
    NoKey,
    #[error("username clash")]
    UsernameClash,
    #[error("not found")]
    NotFound,
    #[error("incorrect username or password")]
    IncorrectUsernameOrPassword,
    #[error("io {0:?}")]
    Io(#[serde(skip)] Option<io::ErrorKind>),
    #[error("api error")]
    Api,
    #[error("serde json {0:?}")]
    SerdeJson(#[serde(skip)] Option<serde_json::error::Category>),
    #[error("serde yaml")]
    SerdeYaml,
    #[error("environment variable")]
    EnvironmentVariable,
    #[error("reqwest")]
    Reqwest,
    #[error("invalid response")]
    InvalidResponse,
    #[error("config error")]
    ConfigError,
    #[error("rsa error")]
    Rsa,
    #[error("invalid session")]
    InvalidSession,
    #[error("invalid data")]
    InvalidData,
    #[error("zbus")]
    Zbus,
    #[error("error accessing local storage")]
    LocalStorage,
}

impl Error {
    pub const fn no_client() -> Self {
        Self {
            err: ErrorEnum::NoClient,
            category: Category::NoClient,
        }
    }

    pub const fn no_key() -> Self {
        Self {
            err: ErrorEnum::NoKey,
            category: Category::NoKey,
        }
    }

    pub const fn invalid_session() -> Self {
        Self {
            err: ErrorEnum::InvalidSession,
            category: Category::InvalidSession,
        }
    }

    pub const fn username_clash() -> Self {
        Self {
            err: ErrorEnum::UsernameClash,
            category: Category::UsernameClash,
        }
    }

    pub const fn username_not_found() -> Self {
        Self {
            err: ErrorEnum::UsernameNotFound,
            category: Category::NotFound,
        }
    }

    pub const fn incorrect_username_or_password() -> Self {
        Self {
            err: ErrorEnum::IncorrectUsernameOrPassword,
            category: Category::IncorrectUsernameOrPassword,
        }
    }

    pub const fn category(&self) -> Category {
        self.category
    }

    pub const fn local_storage() -> Self {
        Self {
            err: ErrorEnum::LocalStorage,
            category: Category::LocalStorage,
        }
    }
}

impl From<io::Error> for Error {
    fn from(them: io::Error) -> Self {
        Self {
            category: Category::Io(Some(them.kind())),
            err: ErrorEnum::Io(them),
        }
    }
}

impl From<serde_json::Error> for Error {
    fn from(them: serde_json::Error) -> Self {
        Self {
            category: Category::SerdeJson(Some(them.classify())),
            err: ErrorEnum::SerdeJson(them),
        }
    }
}

impl From<serde_yaml::Error> for Error {
    fn from(them: serde_yaml::Error) -> Self {
        Self {
            category: Category::SerdeYaml,
            err: ErrorEnum::SerdeYaml(them),
        }
    }
}

impl From<crate::config::ConfigError> for Error {
    fn from(them: crate::config::ConfigError) -> Self {
        Self {
            category: Category::ConfigError,
            err: ErrorEnum::ConfigError(them),
        }
    }
}

impl From<ZeroBytesOverStream> for Error {
    fn from(them: ZeroBytesOverStream) -> Self {
        Self {
            category: Category::Io(Some(io::ErrorKind::UnexpectedEof)),
            err: ErrorEnum::Io(io::Error::new(
                io::ErrorKind::UnexpectedEof,
                them,
            )),
        }
    }
}

impl From<env::VarError> for Error {
    fn from(them: env::VarError) -> Self {
        Self {
            category: Category::EnvironmentVariable,
            err: ErrorEnum::Var(them),
        }
    }
}

impl From<reqwest::Error> for Error {
    fn from(them: reqwest::Error) -> Self {
        Self {
            category: Category::Reqwest,
            err: ErrorEnum::Reqwest(them),
        }
    }
}

impl From<rsa::errors::Error> for Error {
    fn from(them: rsa::errors::Error) -> Self {
        Self {
            category: Category::Rsa,
            err: ErrorEnum::Rsa(them),
        }
    }
}

impl From<rsa::pkcs8::Error> for Error {
    fn from(them: rsa::pkcs8::Error) -> Self {
        Self {
            category: Category::Rsa,
            err: ErrorEnum::DecodeRsa(them),
        }
    }
}

#[cfg(feature = "zbus")]
impl From<zbus::Error> for Error {
    fn from(them: zbus::Error) -> Self {
        Self {
            category: Category::Zbus,
            err: ErrorEnum::Zbus(them),
        }
    }
}

impl From<chrono::format::ParseError> for Error {
    fn from(them: chrono::format::ParseError) -> Self {
        Self {
            category: Category::InvalidData,
            err: ErrorEnum::Chrono(them),
        }
    }
}

impl From<url::ParseError> for Error {
    fn from(them: url::ParseError) -> Self {
        Self {
            category: Category::InvalidData,
            err: ErrorEnum::Url(them),
        }
    }
}

impl From<crate::rsa_key_cache::GetRsaKeyError> for Error {
    fn from(them: crate::rsa_key_cache::GetRsaKeyError) -> Self {
        use crate::rsa_key_cache::GetRsaKeyError;
        match them {
            GetRsaKeyError::Api(v) => v.into(),
            GetRsaKeyError::RsaKey(v) => v.into(),
        }
    }
}

impl From<nominatim::error::Error> for Error {
    fn from(them: nominatim::error::Error) -> Self {
        use nominatim::error::Error;
        match them {
            Error::Reqwest(e) => e.into(),
            Error::Url(e) => e.into(),
            Error::Serde(e) => e.into(),
            Error::ResponseCode(e) => Self {
                category: Category::InvalidResponse,
                err: ErrorEnum::ResponseError {
                    err_type: String::new(),
                    status: e,
                    content: String::new(),
                },
            },
        }
    }
}

impl From<crate::gps::DecryptError> for Error {
    fn from(them: crate::gps::DecryptError) -> Self {
        use crate::gps::DecryptError;

        match them {
            DecryptError::LongitudeInvalidFormat => Self {
                category: Category::InvalidData,
                err: ErrorEnum::LongitudeInvalidFormat,
            },
            DecryptError::LatitudeInvalidFormat => Self {
                category: Category::InvalidData,
                err: ErrorEnum::LatitudeInvalidFormat,
            },
            DecryptError::ChronoParseErr(them) => them.into(),
            DecryptError::RsaError(them) => them.into(),
        }
    }
}

macro_rules! api_error_from_impl {
    ($($typ:ident { $($code:pat => $err:expr),* $(,)?} ),+) => {
        $(
            impl From<ApiError<::api::apis::default_api::$typ>> for Error {
                fn from(them: ApiError<::api::apis::default_api::$typ>) -> Self {
                    match them {
                        ApiError::Reqwest(t) => t.into(),
                        ApiError::Serde(e) => e.into(),
                        ApiError::Io(e) => e.into(),
                        ApiError::ResponseError(t) => match t.status {
                            StatusCode::UNAUTHORIZED => Self::invalid_session(),
                            $(
                                $code => $err,
                            )*
                            _ => {
                                Self {
                                    category: Category::InvalidResponse,
                                    err: ErrorEnum::ResponseError {
                                        err_type: stringify!($typ).to_string(),
                                        status: t.status,
                                        content: t.content,
                                    },
                                }
                            }
                        }
                    }
                }
            }
        )*
    }
}

api_error_from_impl! [
    GpsByRecieverRecieverDeleteError {
        StatusCode::NOT_FOUND => Error::username_not_found(),
    },
    GpsByRecieverRecieverPutError {
    },
    GpsBySenderSenderDeleteError {
        StatusCode::NOT_FOUND => Error::username_not_found(),
    },
    GpsBySenderSenderGetError {
        StatusCode::NOT_FOUND => Error::username_not_found(),
    },
    GpsGetError {},
    GpsPostError {
        StatusCode::CONFLICT => Error::username_clash(),
        StatusCode::NOT_FOUND => Error::username_not_found(),
    },
    LoginLoginPostError {
        StatusCode::FORBIDDEN => Error::incorrect_username_or_password(),
    },
    LoginUsersDeleteError {
        StatusCode::NOT_FOUND => Error::username_not_found(),
    },
    LoginUsersPostError {
        StatusCode::CONFLICT => Error::username_clash(),
    },
    LoginUsersUserRsaKeyGetError {},
    LoginUsersUserRsaKeyPutError {}
];

#[cfg(feature = "serde_errors")]
impl From<ErrorEnum> for ErrorEnumSerde {
    fn from(them: ErrorEnum) -> Self {
        match them {
            ErrorEnum::String(s) => Self::String(s),
            ErrorEnum::NoClient => Self::NoClient,
            ErrorEnum::NoKey => Self::NoKey,
            ErrorEnum::ConfigError(v) => Self::ConfigError(v),
            ErrorEnum::UsernameClash => Self::UsernameClash,
            ErrorEnum::IncorrectUsernameOrPassword => {
                Self::IncorrectUsernameOrPassword
            }
            ErrorEnum::LongitudeInvalidFormat => Self::LongitudeInvalidFormat,
            ErrorEnum::LatitudeInvalidFormat => Self::LatitudeInvalidFormat,
            ErrorEnum::UsernameNotFound => Self::UsernameNotFound,
            ErrorEnum::Io(v) => Self::String(v.to_string()),
            ErrorEnum::SerdeJson(v) => Self::String(v.to_string()),
            ErrorEnum::SerdeYaml(v) => Self::String(v.to_string()),
            ErrorEnum::Reqwest(v) => Self::String(v.to_string()),
            ErrorEnum::Var(v) => Self::String(v.to_string()),
            ErrorEnum::ResponseError {
                err_type,
                status,
                content,
            } => Self::ResponseError {
                err_type,
                status,
                content,
            },
            ErrorEnum::DecodeRsa(v) => Self::String(v.to_string()),
            ErrorEnum::Rsa(v) => Self::String(v.to_string()),
            ErrorEnum::InvalidSession => Self::InvalidSession,
            ErrorEnum::Chrono(v) => Self::String(v.to_string()),
            #[cfg(feature = "zbus")]
            ErrorEnum::Zbus(v) => Self::String(v.to_string()),
            ErrorEnum::LocalStorage => Self::LocalStorage,
            ErrorEnum::Url(s) => Self::String(s.to_string()),
        }
    }
}
