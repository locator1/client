use std::io;
use std::path::Path;
use std::sync::Arc;

use rand::prelude::*;
use rand::rngs::OsRng;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use thiserror::Error;
use tokio::io::AsyncWriteExt;
use tokio::net::UnixStream;

use crate::error::Error as CrateError;

#[derive(Serialize, Deserialize, Debug)]
pub struct Msg<T> {
    pub id: u64,
    pub msg: T,
}

/// A stream in which json information can be transfered to and fro.
///
/// It is a unix socket, no less.
#[derive(Clone)]
pub struct Stream<P: AsRef<Path>> {
    pub path: Arc<P>,
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("io error: {0}")]
    Io(#[from] io::Error),
    #[error("serde error: {0}")]
    Serde(#[from] serde_json::Error),
    #[error("0 bytes when receiving the message")]
    ZeroBytes,
}

impl<P: AsRef<Path>> Stream<P> {
    /// Connects on a given file.
    pub async fn connect(path: P) -> Result<Self, CrateError> {
        // Check the stream exists
        let _conn = UnixStream::connect(&path).await?;
        Ok(Self {
            path: Arc::new(path),
        })
    }

    /// Gets a [`UnixStream`] from this
    pub async fn stream(&self) -> Result<UnixStream, CrateError> {
        UnixStream::connect(&*self.path).await.map_err(Into::into)
    }

    /// Sends and serialised a package as is.
    ///
    /// This should not be used by the user.
    ///
    /// See also:
    ///  - [`send_and_receive`] - sends a message and awaits a response
    ///  - [`send`] - sends a message
    pub async fn send_directly<T: serde::Serialize>(
        &self,
        thing: &Msg<T>,
        stream: &mut UnixStream,
    ) -> Result<(), CrateError> {
        let serialised = serde_json::to_vec(thing)?;
        log::trace!("Sending to stream...");
        loop {
            // Wait for the socket to be writable
            stream.writable().await?;

            match stream.try_write(&serialised) {
                Ok(_n) => return Ok(()),
                Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => continue,
                Err(e) => {
                    return Err(e.into());
                }
            }
        }
    }

    /// Sends some data over the socket.
    pub async fn send<T: serde::Serialize>(
        &self,
        thing: &T,
    ) -> Result<(), CrateError> {
        self.send_directly(
            &Msg { id: 0, msg: thing },
            &mut self.stream().await?,
        )
        .await?;
        Ok(())
    }

    /// Sends some data over a socket and wait for its response.
    pub async fn send_and_receive<T: serde::Serialize, D: DeserializeOwned>(
        &self,
        thing: &T,
    ) -> Result<D, CrateError> {
        let mut rng = OsRng;
        let id: u64 = rng.gen();
        let serialised = &Msg { id, msg: thing };
        let mut stream = self.stream().await?;

        self.send_directly(serialised, &mut stream).await?;
        stream.shutdown().await?;
        log::trace!("Trying to read...");

        let mut buf = Vec::new();

        loop {
            // Wait for the socket to be readable
            stream.readable().await?;
            log::trace!("Stream is ready...");

            // Try to read data, this may still fail with `WouldBlock`
            // if the readiness event is a false positive.
            match stream.try_read_buf(&mut buf) {
                Ok(0) => {
                    eprintln!("{}", String::from_utf8_lossy(&buf));
                    return Ok(serde_json::from_slice(&buf)?);
                }
                Ok(_n) => (),
                Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                    log::trace!("It would block, repeating.");
                    continue;
                }
                Err(e) => {
                    return Err(e.into());
                }
            }
        }
    }
}
