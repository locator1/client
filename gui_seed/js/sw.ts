const staticCacheName = "site-static-v1";
const assets = [
    "../pkg/package.d.ts",
    "../pkg/package.js",
    "../pkg/package.json",
    "../pkg/package_bg.wasm",
    "../pkg/package_bg.wasm.d.ts",
    "../pkg/package.d.ts",
    "/index.html",
    "/css/main.css",
    "/data/icons/org.john_t.Locator.svg",
    "/data/icons/list-add-symbolic.svg",
];
// install event
self.addEventListener("install", (evt: ExtendableEvent) => {
    evt.waitUntil(
        caches.open(staticCacheName).then((cache) => {
            console.log("caching shell assets");
            cache.addAll(assets);
        })
    );
});
// activate event
self.addEventListener("activate", (evt: ExtendableEvent) => {
    evt.waitUntil(
        caches.keys().then((keys) => {
            return Promise.all(
                keys
                    .filter((key) => key !== staticCacheName)
                    .map((key) => caches.delete(key))
            );
        })
    );
});
// fetch event
self.addEventListener("fetch", (evt: FetchEvent) => {
    evt.respondWith(
        caches.match(evt.request).then((cacheRes) => {
            return cacheRes || fetch(evt.request);
        })
    );
});
