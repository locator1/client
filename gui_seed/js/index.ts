import init, { send_geo_request } from "../pkg/package.js";
import { GeolocationSensor } from "./lib/sensor-polyfills/src/geolocation-sensor.js";

var navigator = window.navigator;

async function run() {
    // Try and register for a pwa.
    if ("serviceWorker" in navigator) {
        navigator.serviceWorker
            .register("/js/sw.js")
            .then((reg) => console.log("service worker registered"))
            .catch((err) => console.log("service worker not registered", err));
    }

    await init();

    const geo = new GeolocationSensor({ frequency: 60, accuracy: "high" });
    geo.start();

    geo.onreading = () => {
        send_geo_request(geo.longitude, geo.latitude);
    };
}

run();
