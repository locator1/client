use common::error::Error;
use common::tasks::{self, TaskMode};
use futures::future;
use seed::prelude::JsValue;
use wasm_bindgen::prelude::*;

use super::Msg;

pub async fn get_outgoing_wrapper(tm: &TaskMode) -> Msg {
    let (Ok(x) | Err(x)) = get_outgoing(tm)
        .await
        .map(Msg::UpdateOutgoing)
        .map_err(|x| Msg::SetError(Some(x.to_string())));
    x
}

pub async fn get_outgoing(tm: &TaskMode) -> Result<Vec<String>, Error> {
    log::info!("Getting outgoing connections");

    // Get the username of the current user.
    let username_cfg = tasks::config::get_username(tm).await?;
    let mut conns = tasks::list_connections(tm).await?;
    conns.retain(|conn| Some(&conn.sender) == username_cfg.as_ref());
    let mut fin = Vec::with_capacity(conns.len());
    for conn in conns {
        fin.push(conn.reciever);
    }

    Ok(fin)
}

pub async fn add_outgoing_wrapper(tm: &TaskMode, outgoing: &str) -> Msg {
    let (Ok(x) | Err(x)) = tasks::add_connection(tm, outgoing)
        .await
        .map(|_| Msg::RunOutgoing)
        .map_err(|x| Msg::SetError(Some(x.to_string())));
    x
}

/// This function should not be called within the app. Rather this
/// should be called by `geo.js` service worker
#[wasm_bindgen]
pub async fn send_geo_request(long: f64, lat: f64) -> Result<(), JsValue> {
    send_geo_request_inner(long, lat)
        .await
        .map_err(|x| JsValue::from(x.to_string()))
}

async fn send_geo_request_inner(long: f64, lat: f64) -> Result<(), Error> {
    log::info!("Sending geo request...");

    // Create a gps
    let gps = common::DecryptedGps {
        longitude: Some(long),
        latitude: Some(lat),
        accuracy: None,
        date: None,
    };

    // Create a task mode
    let tm = TaskMode::local().await;

    // Get our username
    let username = tasks::config::get_username(&tm).await?;

    // Get the outgoing connections.
    let mut conns = tasks::list_connections(&tm).await?;
    conns.retain(|conn| Some(&conn.sender) == username.as_ref());

    // Loop through all the connections
    future::try_join_all(conns.into_iter().map(|conn| {
        let tm = &tm;
        let gps = &gps;
        let gps = gps.clone();

        // Return the function which must be called
        async move { tasks::gps::send(tm, gps, &conn.reciever).await }
    }))
    .await?;

    Ok(())
}
