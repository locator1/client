use std::sync::atomic::Ordering;

use seed::{prelude::*, *};

pub(crate) use super::error;
use crate::{Model, Msg};

pub mod connections;
pub mod finish;
pub mod login;
pub mod server;
pub mod signup;

// `view` describes what to display.
pub fn view(model: &Model) -> Node<Msg> {
    div![
        IF!(model.loading.load(Ordering::Relaxed) => C!["loading"]),
        h1!["Locator"],
        server::page1(model),
        signup::signup(model),
        login::login(model),
        connections::connections(model),
        finish::finish(model),
    ]
}

/// Creates a nav bar with a "next" and "back" button which will link
/// to whatever is specified.
fn navbar(next: Option<&str>, back: Option<&str>) -> Node<Msg> {
    div![
        C!["navbar"],
        IF!(
            next.is_some() => a![
                C!["right"],
                "Next",
                attrs!{ At::Href => next.unwrap() },
            ]
        ),
        IF!(
            back.is_some() => a![
                C!["back"],
                "Back",
                attrs!{ At::Href => back.unwrap() },
            ]
        ),
    ]
}

fn input<MsU: 'static>(
    id: &str,
    label: &str,
    typ: &str,
    autocomplete: &str,
    msg: impl FnOnce(String) -> MsU + 'static + Clone,
) -> Node<Msg> {
    div![
        C!["input"],
        label![label, attrs! {At::For => id}],
        input![
            id!(id),
            attrs! {
                At::Type => typ,
                At::AutoComplete => autocomplete,
            },
            input_ev(Ev::Input, msg),
        ],
    ]
}

// TODO: remove me
pub fn input_no_ev(
    id: &str,
    label: &str,
    typ: &str,
    placeholder: &str,
) -> Node<Msg> {
    div![
        C!["input"],
        label![label, attrs! {At::For => id}],
        input![
            id!(id),
            attrs! {
                At::Type => typ,
                At::Placeholder => placeholder,
            },
        ],
    ]
}

// Gives a spinner which will spin
fn spin(model: &Model) -> Node<Msg> {
    if model.loading.load(Ordering::Relaxed) {
        div![C!["spin"]]
    } else {
        empty!()
    }
}
