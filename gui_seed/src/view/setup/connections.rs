use seed::{prelude::*, *};

use super::{error, input, navbar};
use crate::{Model, Msg};

/// Constructs a page where the user can add connections to link
/// to their friends/family.
pub fn connections(model: &Model) -> Option<Node<Msg>> {
    IF!(model.url.hash().map(String::as_str) == Some("connections") =>
        div![
            id!("connections"),
            h3!["Connections"],
            error(model),
            div![
                C!["listbox"],
                create_connections(model),
            ],
            button![
                img!(attrs!{ At::Src => "/data/icons/list-add-symbolic.svg"}),
                C!["symbolic-button", "add_button"],
                ev(Ev::Click, |_| Msg::ConnectionAdd),
            ],
            button![
                "Finish",
                C!["symbolic-button", "add_button"],
                ev(Ev::Click, |_| Msg::ConnectionsFinish),
            ],
            navbar(None, None),
        ]
    )
}

pub fn create_connections(model: &Model) -> Vec<Node<Msg>> {
    let mut vec = Vec::new();
    for (i, _) in model.setup_connections.borrow().iter().enumerate() {
        vec.push(div![
            C!["listbox-row"],
            input![
                attrs! {
                    At::Type => "text",
                    At::Value => model.setup_connections.borrow().get(i).map(String::as_str).unwrap_or_default(),
                },
                input_ev(Ev::Input, move |string| Msg::ConnectionModify(i, string) )
            ],
            button![
                C!["rounded-button", "symbolic-button"],
                img!(attrs!{ At::Src => "/data/icons/edit-delete-symbolic.svg"}),
                ev(Ev::Click, move |_| Msg::ConnectionRemove(i)),
            ]
        ]);
    }
    vec
}
