use seed::{prelude::*, *};

use super::{error, navbar};
use crate::{Model, Msg};

/// Construct the page in which the user finishes setup
pub fn finish(model: &Model) -> Option<Node<Msg>> {
    IF!(model.url.hash().map(String::as_str) == Some("finish") =>
        div![
            id!("finish"),
            h3!["Finish"],
            button!(
                "Finish",
                C!["add_button"],
                ev(Ev::Click, |_| Msg::FinishFinish),
            ),
            error(model),
            navbar(None, Some("#connections")),
        ]
    )
}
