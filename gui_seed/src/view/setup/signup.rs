use seed::{prelude::*, *};

use super::{error, input, navbar, spin};
use crate::{Model, Msg};

/// Constructs a page where the user can sign up for this product.
pub fn signup(model: &Model) -> Option<Node<Msg>> {
    IF!(model.url.hash().map(String::as_str) == Some("signup") =>
        div![
            id!("signup"),
            h3!["Signup"],
            a![
                "Or log in instead",
                attrs!{
                    At::Href => "#login",
                }
            ],
            input("signup_username", "Username:", "text", "username", Msg::UsernameChanged),
            input("signup_password", "Password:", "password", "new-password", Msg::PasswordChanged),
            input("signup_password_confirm", "Confirm Password:", "password", "new-password", |_| Msg::Null),
            IF!(passwords_matching() && password_long_enough() && username_not_empty() => button![
                "Signup",
                ev(Ev::Click, |_| Msg::Signup),
            ]),
            IF!(not(passwords_matching()) => p![
                "Passwords do not match",
                C!["error"],
            ]),
            IF!(not(password_long_enough()) => p![
                "Password must be at least 8 characters",
                C!["error"],
            ]),
            IF!(not(username_not_empty()) => p![
                "Username cannot be empty",
                C!["error"],
            ]),
            error(model),
            spin(model),
            navbar(None, Some("#page1")),
        ]
    )
}

fn passwords_matching() -> bool {
    let closure = || {
        use web_sys::HtmlInputElement;

        // Try and get the password box.
        let password: HtmlInputElement =
            JsValue::from(document().get_element_by_id("signup_password")?)
                .into();

        // Try and get the password box.
        let password_confirmation: HtmlInputElement = JsValue::from(
            document().get_element_by_id("signup_password_confirm")?,
        )
        .into();

        Some(password.value() == password_confirmation.value())
    };

    closure().unwrap_or(true)
}

fn password_long_enough() -> bool {
    let closure = || {
        use web_sys::HtmlInputElement;

        // Try and get the password box.
        let password: HtmlInputElement =
            JsValue::from(document().get_element_by_id("signup_password")?)
                .into();

        Some(password.value().len() >= 8)
    };

    closure().unwrap_or(false)
}

fn username_not_empty() -> bool {
    let closure = || {
        use web_sys::HtmlInputElement;

        // Try and get the password box.
        let username: HtmlInputElement =
            JsValue::from(document().get_element_by_id("signup_username")?)
                .into();

        Some(!username.value().is_empty())
    };

    closure().unwrap_or(false)
}
