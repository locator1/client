use seed::{prelude::*, *};

use super::{error, input, navbar, spin};
use crate::{Model, Msg};

/// Constructs a page where the user can login for this product.
pub fn login(model: &Model) -> Option<Node<Msg>> {
    IF!(model.url.hash().map(String::as_str) == Some("login") =>
        div![
            id!("login"),
            h3!["Login"],
            a![
                "Or Signup instead",
                attrs!{
                    At::Href => "#signup",
                }
            ],
            input("login_username", "Username:", "text", "username", Msg::UsernameChanged),
            input("login_password", "Password:", "password", "existing-password", Msg::PasswordChanged),
            IF!(allow_login() => button![
                "Login",
                ev(Ev::Click, |_| Msg::Login),
            ]),
            error(model),
            spin(model),
            navbar(None, Some("#page1")),
        ]
    )
}

fn allow_login() -> bool {
    let closure = || {
        use web_sys::HtmlInputElement;

        // Try and get the password box.
        let password: HtmlInputElement =
            JsValue::from(document().get_element_by_id("login_password")?)
                .into();

        // Try and get the username box.
        let username: HtmlInputElement =
            JsValue::from(document().get_element_by_id("login_username")?)
                .into();

        Some(!password.value().is_empty() && !username.value().is_empty())
    };

    closure().unwrap_or(true)
}
