use seed::{prelude::*, *};

use super::{input, navbar};
use crate::{Model, Msg};

/// Constructs page 1, the server page.
pub fn page1(model: &Model) -> Option<Node<Msg>> {
    IF!(model.url.hash().map(String::as_str) == Some("page1") || model.url.hash() == None =>
        div![
            id!("page1"),
            h3!["Server configuration"],
            input("server_entry", "Server address:", "url", "url", Msg::ServerChanged),
            navbar(Some("#signup"), None),
        ]
    )
}
