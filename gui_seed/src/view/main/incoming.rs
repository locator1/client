use chrono::prelude::*;
use enclose::enclose;
use seed::{prelude::*, *};

use crate::{view::error, Model, Msg};

pub fn view(model: &Model) -> Node<Msg> {
    let f = timeago::Formatter::new();
    let now = Utc::now();
    div![
        id!("incoming"),
        h3!("Incoming"),
        error(model),
        div![C!["listbox"], {
            let mut nodes = Vec::with_capacity(model.incoming.len());

            for (k, v) in &model.incoming {
                let lon = v.gps.longitude;
                let lat = v.gps.latitude;
                let dur = v.gps.date.map(|x| f.convert_chrono(x, now));
                nodes.push(div![
                        C!["listbox-row"],

                        // Make the main div, which has the title and
                        // location
                        button![
                            C!["seamless-button"],

                            // Title
                            strong![k],

                            // Coordinates
                            p![format!(
                                "({}, {})",
                                lat.map(|x| x.to_string()).as_deref().unwrap_or("???"),
                                lon.map(|x| x.to_string()).as_deref().unwrap_or("???"),
                            )],

                            // Nominatim
                            if let Some(nominatim) = &v.nominatim {
                                if let Some(display_name) = &nominatim.display_name {
                                    p![display_name]
                                } else {
                                    empty!()
                                }
                            } else {
                                empty!()
                            },

                            // Last seen
                            p![format!("Last seen {}", dur.as_deref().unwrap_or("???"))],
                            ev(Ev::Click, enclose!((k) move |_| Msg::IncomingNominatim(k))),
                        ],

                        // Try and create a button to bring to another app.
                        if let (Some(long), Some(lat)) = (lon, lat) {
                            a![
                                C!["rounded-button", "symbolic-button", "button", "flex-center"],
                                img!(attrs!{ At::Src => "/data/icons/external-link-symbolic.svg"}),
                                attrs!{ At::Href => &create_geo_link(long, lat, k) }
                            ]
                        } else {
                            empty!()
                        },
                    ])
            }
            nodes
        }]
    ]
}

pub fn create_geo_link(longitude: f64, latitude: f64, person: &str) -> String {
    let user_agent = web_sys::window()
        .unwrap()
        .navigator()
        .user_agent()
        .unwrap_or_default();

    if user_agent.contains("iPhone") {
        format!(
            "https://maps.apple.com?ll={},{}&q={}",
            latitude, longitude, person
        )
    } else {
        format!("geo:{},{}", latitude, longitude)
    }
}
