use seed::{prelude::*, *};

use crate::{view::error, Model, Msg};

pub fn view(model: &Model) -> Node<Msg> {
    div![
        id!("outgoing"),
        h3!("Outgoing"),
        error(model),
        div![
            C!["listbox"],
            {
                let mut nodes = Vec::with_capacity(model.outgoing.len());

                for (i, outgoing) in model.outgoing.iter().enumerate() {
                    nodes.push(div![
                        C!["listbox-row"],
                        span![outgoing],
                        button![
                            C!["rounded-button", "symbolic-button", "flex-center"],
                            img!(attrs!{ At::Src => "/data/icons/edit-delete-symbolic.svg"}),
                            ev(Ev::Click, move |_| Msg::OutgoingDelete(i)),
                        ]
                    ]);
                }

                nodes
            },
            div![
                C!["listbox-row", "dim-when-unfocused", "center-text"],
                input![
                    attrs! {
                        At::Type => "text",
                        At::Value => &model.outgoing_uncreated,
                    },
                    input_ev(Ev::Input, Msg::OutgoingUncreatedModify)
                ],
                button![
                    C!["rounded-button", "symbolic-button", "flex-center"],
                    img!(
                        attrs! { At::Src => "/data/icons/list-add-symbolic.svg"}
                    ),
                    ev(Ev::Click, |_| Msg::OutgoingUncreatedAdd),
                ]
            ]
        ]
    ]
}
