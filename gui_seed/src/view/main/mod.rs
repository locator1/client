use crate::{Model, Msg};
pub mod incoming;
pub mod outgoing;
use seed::{prelude::*, *};

pub fn view(model: &Model) -> Node<Msg> {
    div![
        IF!(
            model.url.hash().map(String::as_str) == Some("incoming") || model.url.hash() == None =>
            incoming::view(model)
        ),
        IF!(
            model.url.hash().map(String::as_str) == Some("outgoing") =>
            outgoing::view(model)
        ),
        div![
            C!["stack_switcher"],
            stack_button("Incoming", "download-symbolic.svg", "#incoming"),
            stack_button("Outgoing", "share-symbolic.svg", "#outgoing"),
        ],
    ]
}

pub fn stack_button(text: &str, icon: &str, href: &str) -> Node<Msg> {
    a![
        attrs! { At::Href => href },
        span![
            C!["stack_button"],
            div![
                C!["flex", "symbolic-button"],
                img!(attrs! { At::Src => format!("/data/icons/{}", icon)}),
            ],
            span![text,],
        ]
    ]
}
