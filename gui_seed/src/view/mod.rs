pub mod main;
pub mod setup;
use std::sync::atomic::Ordering;

use seed::{prelude::*, *};

use crate::{Model, Msg};

pub fn view(model: &Model) -> Node<Msg> {
    log::info!("Running view function");

    if model.in_setup.load(Ordering::Relaxed) {
        setup::view(model)
    } else {
        main::view(model)
    }
}

/// Creates an error label
pub fn error(model: &Model) -> Node<Msg> {
    let error = model.error.borrow();
    p![error.as_deref().unwrap_or_default(), C!["error"]]
}
