use common::error::Error;
use common::tasks::{self, config::ConfigMask, TaskMode};
use rand::rngs::OsRng;
use rsa::RsaPrivateKey;

pub async fn login(tm: &mut TaskMode) -> Result<(), Error> {
    // Generate an RSA key.
    let mut rng = OsRng;
    let bits: usize = 2048;
    let rsa_key = RsaPrivateKey::new(&mut rng, bits)?;

    // Set the RSA key.
    let config = tasks::config::get(tm, ConfigMask::PRIVATEKEY).await?;
    if config.private_key.is_none() {
        tasks::config::mod_private_key(tm, Some(rsa_key)).await?;
    }

    tasks::login(tm, true).await?;

    Ok(())
}
