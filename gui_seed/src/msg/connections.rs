use std::cell::RefCell;
use std::rc::Rc;

use common::error::Error;
use common::tasks::{self, TaskMode};

pub async fn post_connections(
    tm: &TaskMode,
    conns: &Rc<RefCell<Vec<String>>>,
) -> Result<(), Error> {
    let conns = conns.borrow();

    // Delete the previous connections
    tasks::connections::remove_current_connections(tm).await?;

    // Loop through every connection and add them separately.
    //
    // This will exit at the first failed attempt.
    futures::future::try_join_all(conns.iter().map(|conn| {
        let tm = &tm;

        // Return the function which must be called
        tasks::add_connection(tm, conn)
    }))
    .await?;

    Ok(())
}
