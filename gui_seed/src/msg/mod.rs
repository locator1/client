use std::collections::HashMap;
use std::mem;

use common::{
    tasks::{self, config::ConfigMask, TaskMode},
    Config,
};
use seed::prelude::*;

use crate::model::GpsAndNominatim;
use crate::Model;

pub mod connections;
pub mod login;
pub mod signup;

#[derive(Debug, Clone)]
pub enum Msg {
    /// A message just to call an update
    Null,

    // Goes to the main ui the page
    ToMainUI,

    // Runs functions to do with the main UI.
    MainUIStart,

    /// Sets the errror
    SetError(Option<String>),

    // ---- General ----
    UrlChanged(subs::UrlChanged),

    // ---- Setup ----

    // Page 1
    ServerChanged(String),

    // Signup / Login
    UsernameChanged(String),
    PasswordChanged(String),
    Signup,
    Login,
    ToConnections,

    // Connections
    ConnectionAdd,
    ConnectionRemove(usize),
    ConnectionModify(usize, String),
    ConnectionsFinish,
    ToFinish,

    // Finish
    FinishFinish,

    // Incoming
    RunIncoming,
    UpdateIncoming(HashMap<String, GpsAndNominatim>),
    IncomingNominatim(String),
    UpdateNominatim(String, Box<Option<nominatim::Response>>),

    // Outgoing
    UpdateOutgoing(Vec<String>),
    RunOutgoing,
    OutgoingUncreatedModify(String),
    OutgoingUncreatedAdd,
    OutgoingDelete(usize),
}

// `update` describes how to handle each `Msg`.
pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    log::info!("Running update function {:#?}", msg);
    match msg {
        Msg::Null => (),
        Msg::ToMainUI => {
            orders.request_url(Url::current().set_hash(""));
            web_sys::window().unwrap().location().reload().unwrap();
        }
        Msg::MainUIStart => {
            let task_mode = model.task_mode.clone();
            orders.perform_cmd(async move {
                log::info!("Acquiring client...");
                let (Ok(x) | Err(x)) = tasks::acquire_client(&task_mode)
                    .await
                    .map_err(|x| Msg::SetError(Some(x.to_string())))
                    .map(|_x| Msg::RunIncoming);
                log::info!("Acquired client.");
                x
            });
            orders.stream(streams::interval(1000 * 30, || Msg::RunIncoming));
            orders.send_msg(Msg::RunOutgoing);
        }
        Msg::UrlChanged(_) => {
            *model.error.borrow_mut() = None;
            model.url = Url::current();
        }
        Msg::ServerChanged(string) => {
            let task_mode = model.task_mode.clone();
            orders.perform_cmd(async move {
                let config = Config {
                    server: Some(string),
                    ..Config::default()
                };

                tasks::client_with_config(&task_mode, config)
                    .await
                    .map_err(|x| Msg::SetError(Some(x.to_string())))
                    .err()
            });
        }
        Msg::UsernameChanged(string) => {
            let task_mode = model.task_mode.clone();
            orders.perform_cmd(async move {
                let config = Config {
                    username: Some(string),
                    ..Config::default()
                };

                tasks::mod_config(&task_mode, ConfigMask::USERNAME, config)
                    .await
                    .map_err(|x| Msg::SetError(Some(x.to_string())))
                    .err()
            });
        }
        Msg::PasswordChanged(string) => {
            let task_mode = model.task_mode.clone();
            orders.perform_cmd(async move {
                let config = Config {
                    password: Some(string),
                    ..Config::default()
                };

                tasks::mod_config(&task_mode, ConfigMask::PASSWORD, config)
                    .await
                    .map_err(|x| Msg::SetError(Some(x.to_string())))
                    .err()
            });
        }
        Msg::Signup => {
            let mut task_mode = model.task_mode.clone();
            orders.perform_cmd(async move {
                let (Ok(x) | Err(x)) = signup::signup(&mut task_mode)
                    .await
                    .map_err(|x| Msg::SetError(Some(x.to_string())))
                    .map(|_x| Msg::ToConnections);
                x
            });
        }
        Msg::Login => {
            let mut task_mode = model.task_mode.clone();
            orders.perform_cmd(async move {
                let (Ok(x) | Err(x)) = login::login(&mut task_mode)
                    .await
                    .map_err(|x| Msg::SetError(Some(x.to_string())))
                    .map(|_x| Msg::ToConnections);
                x
            });
        }
        Msg::ToConnections => {
            orders.request_url(Url::current().set_hash("connections"));
        }
        Msg::ConnectionAdd => {
            log::trace!("Adding a connection");
            model.setup_connections.borrow_mut().push(String::new())
        }
        Msg::ConnectionRemove(i) => {
            log::trace!("Removing connection {}", i);
            model.setup_connections.borrow_mut().remove(i);
        }
        Msg::ConnectionModify(i, string) => {
            *model.setup_connections.borrow_mut().get_mut(i).unwrap() = string
        }
        Msg::ConnectionsFinish => {
            let task_mode = model.task_mode.clone();
            let connections = model.setup_connections.clone();
            orders.perform_cmd(async move {
                let (Ok(x) | Err(x)) =
                    connections::post_connections(&task_mode, &connections)
                        .await
                        .map_err(|x| Msg::SetError(Some(x.to_string())))
                        .map(|_x| Msg::ToFinish);
                x
            });
        }
        Msg::ToFinish => {
            log::info!("Going to finish page");
            orders.request_url(Url::current().set_hash("finish"));
        }
        Msg::FinishFinish => {
            let task_mode = model.task_mode.clone();
            orders.perform_cmd(async move {
                let (Ok(x) | Err(x)) = tasks::write_config(&task_mode)
                    .await
                    .map_err(|x| Msg::SetError(Some(x.to_string())))
                    .map(|_x| Msg::ToMainUI);
                x
            });
        }
        Msg::UpdateIncoming(hashmap) => {
            dbg!(&hashmap);
            model.incoming = hashmap;
        }
        Msg::SetError(string) => {
            *model.error.borrow_mut() = string;
        }
        Msg::RunIncoming => {
            let tm = TaskMode::clone(&model.task_mode);
            orders.perform_cmd(async move {
                crate::incoming::get_incoming_wrapper(&tm).await
            });
        }
        Msg::IncomingNominatim(string) => {
            let tm = TaskMode::clone(&model.task_mode);
            if let Some(gps) = model.incoming.get(&string) {
                // Get the longitude and latitude
                let lonlat = (gps.gps.longitude, gps.gps.latitude);

                // Check if they are both existant.
                if let (Some(lon), Some(lat)) = lonlat {
                    // Find their nominatim
                    orders.perform_cmd(async move {
                        let (Ok(x) | Err(x)) =
                            tasks::nominatim::reverse_geocode(&tm, lon, lat)
                                .await
                                .map(|x| {
                                    Msg::UpdateNominatim(
                                        string,
                                        Box::new(Some(x)),
                                    )
                                })
                                .map_err(|x| {
                                    Msg::SetError(Some(x.to_string()))
                                });
                        x
                    });
                }
            } else {
                log::error!("WHERE is {}!?", string);
            }
        }
        Msg::UpdateNominatim(key, response) => {
            if let Some(x) = model.incoming.get_mut(&key) {
                x.nominatim = *response;
            }
        }
        Msg::UpdateOutgoing(vec) => {
            model.outgoing = vec;
            orders.send_msg(Msg::SetError(None));
        }
        Msg::RunOutgoing => {
            let tm = TaskMode::clone(&model.task_mode);
            orders.perform_cmd(async move {
                crate::outgoing::get_outgoing_wrapper(&tm).await
            });
        }
        Msg::OutgoingUncreatedModify(text) => {
            model.outgoing_uncreated = text;
        }
        Msg::OutgoingUncreatedAdd => {
            let outgoing = mem::take(&mut model.outgoing_uncreated);
            let tm = TaskMode::clone(&model.task_mode);
            orders.perform_cmd(async move {
                crate::outgoing::add_outgoing_wrapper(&tm, &outgoing).await
            });
        }
        Msg::OutgoingDelete(pos) => {
            let outgoing = mem::take(model.outgoing.get_mut(pos).unwrap());
            let tm = TaskMode::clone(&model.task_mode);
            orders.perform_cmd(async move {
                let (Ok(x) | Err(x)) =
                    tasks::remove_connection_from_reciever(&tm, outgoing)
                        .await
                        .map(|_| Msg::RunOutgoing)
                        .map_err(|x| Msg::SetError(Some(x.to_string())));
                x
            });
        }
    }
    log::info!("Completed update function");
}
