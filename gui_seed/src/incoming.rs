use std::collections::HashMap;

use common::error::Error;
use common::tasks::{self, config::ConfigMask, TaskMode};

use super::Msg;
use crate::model::GpsAndNominatim;

pub async fn get_incoming_wrapper(tm: &TaskMode) -> Msg {
    let (Ok(x) | Err(x)) = get_incoming(tm)
        .await
        .map(Msg::UpdateIncoming)
        .map_err(|x| Msg::SetError(Some(x.to_string())));
    x
}

pub async fn get_incoming(
    task_mode: &TaskMode,
) -> Result<HashMap<String, GpsAndNominatim>, Error> {
    log::info!("Getting incoming connections");
    let conns_raw = tasks::list_connections(task_mode).await?;

    // Gets the current username so we can filter by it.
    let username_cfg =
        tasks::config::get(task_mode, ConfigMask::USERNAME).await?;

    // Blank HashMap to insert into.
    let mut hashmap = HashMap::new();

    for conn in conns_raw {
        if Some(&conn.sender) != username_cfg.username.as_ref() {
            let gps = tasks::gps::get(task_mode, &conn.sender).await?;
            hashmap.insert(
                conn.sender,
                GpsAndNominatim {
                    gps,
                    nominatim: None,
                },
            );
        }
    }

    Ok(hashmap)
}
