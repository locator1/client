use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;
use std::sync::{atomic::AtomicBool, Arc};

use common::tasks::TaskMode;
use common::DecryptedGps;

// `Model` describes our app state.
pub struct Model {
    pub in_setup: Arc<AtomicBool>,
    pub task_mode: TaskMode,
    pub base_url: seed::Url,
    pub url: seed::Url,
    pub loading: Arc<AtomicBool>,
    pub error: Rc<RefCell<Option<String>>>,
    pub setup_connections: Rc<RefCell<Vec<String>>>,
    pub incoming: HashMap<String, GpsAndNominatim>,
    pub outgoing: Vec<String>,
    pub outgoing_uncreated: String,
}

#[derive(Debug, Clone)]
pub struct GpsAndNominatim {
    pub gps: DecryptedGps,
    pub nominatim: Option<nominatim::Response>,
}
