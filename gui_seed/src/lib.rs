// (Lines like the one below ignore selected Clippy rules
//  - it's useful when you want to check your code with `cargo make verify`
// but some rules are too "annoying" or are not applicable for your case.)
#![allow(clippy::wildcard_imports)]
#![deny(unused_must_use)]

use common::tasks::TaskMode;
use seed::prelude::*;
pub mod msg;
pub mod view;
pub use msg::Msg;
pub mod model;
use std::sync::Arc;

pub use model::Model;
pub mod incoming;
pub mod outgoing;
#[macro_use]
pub mod util;

pub static JS_PATH: &str = "/pkg/package.js";

// `init` describes what should happen when your app started.
fn init(url: Url, orders: &mut impl Orders<Msg>) -> Model {
    // Make sure we know when the url changes
    orders.subscribe(Msg::UrlChanged).notify(subs::UrlChanged);

    let in_setup = util::in_setup();
    let task_mode = TaskMode::local_not_logged_in();
    orders.send_msg(Msg::MainUIStart);

    // Create the model
    Model {
        task_mode,
        base_url: url.to_base_url(),
        url: url.to_base_url(),
        loading: Arc::new(false.into()),
        error: Default::default(),
        setup_connections: Default::default(),
        in_setup: Arc::new(in_setup.into()),
        incoming: Default::default(),
        outgoing: Vec::with_capacity(0),
        outgoing_uncreated: String::with_capacity(0),
    }
}

// (This function is invoked by `init` function in `index.html`.)
#[wasm_bindgen(start)]
pub fn start() {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    console_log::init_with_level(log::Level::Trace).unwrap();

    // Mount the `app` to the element with the `id` "app".
    App::start("app", init, msg::update, view::view);
}
