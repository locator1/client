use rand::rngs::OsRng;
use rsa::RsaPrivateKey;
use seed::prelude::*;

/// Generates an RSA key.
///
/// Currently this function is incredibly simple and doesn't really
/// warrant its own function its just because later this function
/// should be made paralell.
///
/// TODO: Make this function paralell with rayon?
pub fn generate_rsa_key() -> RsaPrivateKey {
    let mut rng = OsRng;
    RsaPrivateKey::new(&mut rng, 2048).unwrap()
}

#[macro_export]
macro_rules! wrapper {
    ($func:path, $ok:path, $err:path, $model:ident, $orders:ident $(,)?) => {
        let loading = $model.loading.clone();
        loading.store(true, Ordering::Relaxed);
        let client = $model.client.clone();
        let error = $model.error.clone();
        $orders.perform_cmd(async move {
            crate::wrapper_fn!($func, $ok, $err, client, loading, error).await
        });
        log::trace!("wrapper completed");
    };
}

#[macro_export]
macro_rules! wrapper_fn {
    (
        $func:path,
        $ok:path,
        $err:path,
        $client:ident,
        $loading:ident,
        $error:ident
    ) => {
        async move {
            let op = $func(&$client).await;
            *$error.borrow_mut() = op.as_ref().err().map(ToString::to_string);
            $loading.store(false, ::std::sync::atomic::Ordering::Relaxed);
            log::trace!("wrapper_fn completed");
            if op.is_ok() {
                $ok
            } else {
                $err
            }
        }
    };
}

/// Works out wether we are in setup. We are not in setup if we
/// have a username, password, RSA key and server.
pub fn in_setup() -> bool {
    let (Ok(x) | Err(x)) = LocalStorage::get(common::ID)
        .map(|_: common::Config| true)
        .map_err(|_| false);
    !x
}
