# Locator

Locator is a location sharing app to share location with friends. It is designed
to be self hosted although eventually I will create a service which you can pay
for. It is end to end encrypted using RSA keys.
